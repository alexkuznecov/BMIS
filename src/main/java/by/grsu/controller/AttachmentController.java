package by.grsu.controller;

import by.grsu.dto.AttachmentDTO;
import by.grsu.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alek on 1.5.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/attach")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @RequestMapping("/add")
    public Boolean add(@RequestBody AttachmentDTO attachmentDTO) {
        return attachmentService.add(attachmentDTO);
    }
}
