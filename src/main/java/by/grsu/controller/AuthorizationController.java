package by.grsu.controller;

import by.grsu.dto.RegistrationDataDTO;
import by.grsu.dto.TokenDTO;
import by.grsu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by alek on 6.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/au")
public class AuthorizationController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public TokenDTO auth(@RequestHeader("login") String login, @RequestHeader("password") String password) {
        return userService.checkUser(login, password);
    }


    @RequestMapping("/reg")
    public Boolean register(@RequestBody RegistrationDataDTO registrationData) {
        userService.add(registrationData.getName(), registrationData.getLastName(), registrationData.getEmail(),
                registrationData.getLoginName(), registrationData.getPassword());
        return true;
    }

    @RequestMapping("/check")
    public Boolean checkLogin(@RequestBody String login) {
        return userService.checkLogin(login);
    }

    @RequestMapping("/logout")
    public Boolean logout(@RequestHeader("auth-token") String uuid) {
        return userService.logout(uuid);
    }

    @RequestMapping("/passrec")
    public Boolean recoveryPassword(@RequestHeader("login") String login) {
        return userService.passwordRecovery(login);
    }
}
