package by.grsu.controller;

import by.grsu.dto.FileDTO;
import by.grsu.entity.*;
import by.grsu.entity.File;
import by.grsu.enums.FileType;
import by.grsu.service.FileService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletInputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;
import java.io.*;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by alek on 22.3.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/files")
@MultipartConfig(fileSizeThreshold = 20971520)
public class FileTransferController {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(FileTransferController.class);

    @Autowired
    private FileService fileService;

    @Deprecated
    @RequestMapping(value = "/upload1", method = RequestMethod.POST)
    @Produces("application/json;charset=UTF-8")
    public String continueFileUpload(HttpServletRequest request, HttpServletResponse response){String path = "/home/alek/up/";
        ServletInputStream reader = null;
        FileOutputStream writer = null;
        try {
            reader = request.getInputStream();
            String myString = IOUtils.toString(reader, "UTF-8");

            Pattern pattern = Pattern.compile("FormBoundary.*?(.*(Content-Disposition.*filename=\\\"(.*?)\\\").*?(.*?)(.*)).*------WebKitFormBoundary", Pattern.DOTALL | Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(myString);
            String fileEntry = "";

            while (matcher.find()) {
                path += matcher.group(3);
                fileEntry = matcher.group(5);
            }

            fileEntry = fileEntry.replaceAll("(\\r\\nContent.*?\\r\\n\\r\\n)", "");

//            File outputFile = new File(path);
//            writer = new FileOutputStream(outputFile);
//            outputFile.createNewFile();
//            writer.write(fileEntry.getBytes());

        } catch (IOException e) {
            log.error(e.toString());
        } finally {
            try {
                reader.close();
                writer.close();
            } catch (IOException e) {
                log.error(e.toString());
            }
        }
        return "File succesfully uploaded";
    }

    @PostMapping("/upload")
    public String multiFileUpload(@RequestHeader("FILE_TYPE") String fileType,
                                  @RequestHeader("FILE_NAME") String fileName,
                                  @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                if (fileService.saveFile(file, fileName, fileType)) {
                    return "Successful uploading " + fileName;
                } else {
                    return "Failed to upload " + fileName;
                }
            } catch (Exception e) {
                log.error("Failed to upload " + fileName + " => " + e.getMessage());
                return "You failed to upload " + fileName;
            }
        } else {
            return "You failed to upload file, because the file was empty.";
        }
    }

    @PostMapping("/uploadImg")
    public FileDTO imageUpload(@RequestParam("file") MultipartFile file) {
        FileDTO fileDTO = new FileDTO();
        if (!file.isEmpty()) {
            try {
                fileDTO = fileService.saveImage(file, "");
                if (!fileDTO.getId().equals("error")) {
                    return fileDTO;
                } else {
                    fileDTO.setId("Failed to upload ");
                    return fileDTO;
                }
            } catch (Exception e) {
                log.error("Failed to upload " + e.getMessage());
                fileDTO.setId("You failed to upload ");
                return fileDTO;
            }
        } else {
            fileDTO.setId("You failed to upload file, because the file was empty.");
            return fileDTO;
        }
    }

    @RequestMapping("/attfiles")
    public List<FileDTO> getIdAndNameAttachment() {
        try {
            return fileService.getIdAndName(FileType.ATTACHMENT.name());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @RequestMapping("/specfiles")
    public List<FileDTO> getIdAndNameSpectr() {
        try {
            return fileService.getIdAndName(FileType.SPECTR.name());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @RequestMapping("/resfiles")
    public List<FileDTO> getIdAndNameResPass() {
        try {
            return fileService.getIdAndName(FileType.RESEARCH_PASSPORT.name());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @RequestMapping("/images")
    public List<FileDTO> getIdAndNameAttachmentImg() {
        try {
            List<FileDTO> fileDTOS = fileService.getIdAndName(FileType.ATTACHMENT_IMG.name());
            for (FileDTO fileDTO1 : fileDTOS) {
                fileDTO1.setName(null);
            }
            return fileDTOS;
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @RequestMapping("/getImage")
    public String getImageUrlById(@RequestHeader("imgId") String imageId) {
        try {
            String directUrl = fileService.getImage(imageId);
            if (directUrl != null) {
                return directUrl;
            } else {
                return "Director url for this file not found";
            }
        } catch (Exception e) {
            return "Director url for this file not found";
        }
    }
}
