package by.grsu.controller;

import by.grsu.dto.ManufacturersDTO;
import by.grsu.service.ManufacturerService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/manufacturers")
public class ManufacturerController {

    @Autowired
    private ManufacturerService manufacturerService;

    @RequestMapping("/idname")
    public List<ManufacturersDTO> getIdandName() {
        return manufacturerService.getManufactorersIdName();
    }

    @RequestMapping("/add")
    public Boolean add(@RequestBody ManufacturersDTO manufacturersDTO) {
        return manufacturerService.add(manufacturersDTO);
    }

}
