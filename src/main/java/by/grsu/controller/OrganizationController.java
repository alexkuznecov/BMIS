package by.grsu.controller;

import by.grsu.dto.OrganizationDTO;
import by.grsu.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;

    @RequestMapping("/idname")
    public List<OrganizationDTO> getIdandName() {
        return organizationService.getOrganizationsIdName();
    }

    @RequestMapping("/add")
    public Boolean add(@RequestBody OrganizationDTO organizationDTO) {
        return organizationService.add(organizationDTO);
    }
}
