package by.grsu.controller;

import by.grsu.dto.QualityStandartDTO;
import by.grsu.service.QualityStandartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/qstandarts")
public class QualityStandartController {

    @Autowired
    private QualityStandartService qualityStandartService;

    @RequestMapping("/idname")
    public List<QualityStandartDTO> getIdandName() {
        return qualityStandartService.getQualityStandartsIdName();
    }

    @RequestMapping("/add")
    public Boolean add(@RequestBody QualityStandartDTO qualityStandartDTO) {
        return qualityStandartService.add(qualityStandartDTO);
    }

}
