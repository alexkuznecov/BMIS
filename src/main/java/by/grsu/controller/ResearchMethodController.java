package by.grsu.controller;

import by.grsu.dto.ResearchMethodDTO;
import by.grsu.service.ResearchMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/resmethod")
public class ResearchMethodController {

    @Autowired
    private ResearchMethodService researchMethodService;

    @RequestMapping("/add")
    public Boolean add(@RequestBody ResearchMethodDTO researchMethodDTO) {
        return researchMethodService.add(researchMethodDTO);
    }

    @RequestMapping("/idname")
    public List<ResearchMethodDTO> getIdAndName() {
        return researchMethodService.getIdAndName();
    }
}
