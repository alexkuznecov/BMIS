package by.grsu.controller;

import by.grsu.dto.ResearchObjectDTO;
import by.grsu.dto.ResearchObjectTypeDTO;
import by.grsu.entity.ResearchObjectType;
import by.grsu.service.ResearchObjectTypeService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/rotype")
public class ResearchObjectTypeController {

    @Autowired
    private ResearchObjectTypeService researchObjectTypeService;

    @RequestMapping("/idname")
    public List<ResearchObjectTypeDTO> getIdandName() {
        return researchObjectTypeService.getResearchObjectTypesIdName();
    }

    @RequestMapping("/add")
    public Boolean add(@RequestBody ResearchObjectTypeDTO researchObjectTypeDTO) {
        return researchObjectTypeService.add(researchObjectTypeDTO);
    }
}
