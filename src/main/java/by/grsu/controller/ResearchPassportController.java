package by.grsu.controller;

import by.grsu.dto.ResearchPassportDTO;
import by.grsu.service.ResearchPassportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/rpass")
public class ResearchPassportController {

    @Autowired
    private ResearchPassportService researchPassportService;

    @RequestMapping("/idname")
    public List<ResearchPassportDTO> getIdandName() {
        return researchPassportService.getResearchPasportsIdName();
    }

    @RequestMapping("/add")
    public Boolean add(@RequestBody ResearchPassportDTO researchPassportDTO) {
        return researchPassportService.add(researchPassportDTO);
    }
}
