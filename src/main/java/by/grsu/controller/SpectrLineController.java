package by.grsu.controller;

import by.grsu.dto.SpectrLineDTO;
import by.grsu.service.SpectrLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sline")
public class SpectrLineController {

    @Autowired
    private SpectrLineService spectrLineService;

    @RequestMapping("/idname")
    public List<SpectrLineDTO> getIdandName() {
        return spectrLineService.getSpectrLinesIdName();
    }

}
