package by.grsu.controller;

import by.grsu.dto.UserDTO;
import by.grsu.service.UserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by alek on 6.4.17.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/users")
public class UserController {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @RequestMapping("/get")
    public UserDTO getByLogin(@RequestHeader("login") String login) {
        return userService.getByLogin(login);
    }

    @RequestMapping("/edit")
    public Boolean edit(@RequestBody UserDTO userDTO) {
        try {
            return userService.update(userDTO);
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @RequestMapping("/updpass")
    public Boolean updatePassword(@RequestHeader("login") String login, @RequestHeader("password") String password) {
        try{
            return userService.updatePassword(login,password);
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

}
