package by.grsu.converters;

import by.grsu.dto.ChemicalElementsDTO;
import by.grsu.entity.ChemicalElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 13.4.17.
 */
public class ChemicalElementConverter {

    public static List<ChemicalElementsDTO> convertToChemicalElementsDTO(List<ChemicalElement> chemicalElements) {
        List<ChemicalElementsDTO> chemicalElementsDTOS = new ArrayList<>();

        for (ChemicalElement chemicalElement : chemicalElements) {
            ChemicalElementsDTO chemicalElementsDTO = new ChemicalElementsDTO();
            chemicalElementsDTO.setId(chemicalElement.getCeid().toString());
            chemicalElementsDTO.setName(chemicalElement.getName());
            chemicalElementsDTOS.add(chemicalElementsDTO);
        }

        return chemicalElementsDTOS;
    }

}
