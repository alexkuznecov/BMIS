package by.grsu.converters;

import by.grsu.dto.FileDTO;
import by.grsu.entity.File;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
public class FileConverter {

    public static List<FileDTO> convertToFileDTO(List<File> files) {
        List<FileDTO> fileDTOS = new ArrayList<>();

        for (File file : files) {
            FileDTO fileDTO = new FileDTO();
            fileDTO.setId(file.getId().toString());
            fileDTO.setName(file.getAlias());
            if (file.getDirectUrl() != null) {
                fileDTO.setDirectUrl(file.getDirectUrl());
            }
            fileDTOS.add(fileDTO);
        }

        return fileDTOS;
    }

}
