package by.grsu.converters;

import by.grsu.dto.ManufacturersDTO;
import by.grsu.entity.Manufacturer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public class ManufacturerConverter {

    public static List<ManufacturersDTO> convertToManufactorerDTO(List<Manufacturer> manufacturers) {
        List<ManufacturersDTO> manufacturersDTOS = new ArrayList<>();

        for (Manufacturer manufacturer : manufacturers) {
            ManufacturersDTO manufacturersDTO = new ManufacturersDTO();
            manufacturersDTO.setId(manufacturer.getMnfid().toString());
            manufacturersDTO.setName(manufacturer.getName());
            manufacturersDTOS.add(manufacturersDTO);
        }

        return manufacturersDTOS;
    }

}
