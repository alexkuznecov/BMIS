package by.grsu.converters;

import by.grsu.dto.OrganizationDTO;
import by.grsu.entity.Organization;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
public class OrganizationConverter {

    public static List<OrganizationDTO> convertToOrganizationDTO(List<Organization> organizations) {

        List<OrganizationDTO> organizationDTOS = new ArrayList<>();

        for (Organization organization : organizations) {

            OrganizationDTO organizationDTO = new OrganizationDTO();
            organizationDTO.setId(organization.getOid().toString());
            organizationDTO.setName(organization.getFullTitle());
            organizationDTOS.add(organizationDTO);
        }

        return organizationDTOS;
    }

}
