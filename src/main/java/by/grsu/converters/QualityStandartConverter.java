package by.grsu.converters;

import by.grsu.dto.QualityStandartDTO;
import by.grsu.entity.QualityStandart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public class QualityStandartConverter {

    public static List<QualityStandartDTO> convertToQualityStandartDTO(List<QualityStandart> qualityStandarts) {
        List<QualityStandartDTO> qualityStandartDTOS = new ArrayList<>();

        for (QualityStandart qualityStandart : qualityStandarts) {
            QualityStandartDTO qualityStandartDTO = new QualityStandartDTO();
            qualityStandartDTO.setId(qualityStandart.getQsid().toString());
            qualityStandartDTO.setName(qualityStandart.getName());
            qualityStandartDTOS.add(qualityStandartDTO);
        }

        return qualityStandartDTOS;
    }

}
