package by.grsu.converters;

import by.grsu.dto.ResearchMethodDTO;
import by.grsu.entity.ResearchMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
public class ResearchMethodConverter {

    public static List<ResearchMethodDTO> convertToResearchMethodDTO(List<ResearchMethod> researchMethods) {
        List<ResearchMethodDTO> researchMethodDTOS = new ArrayList<>();

        for (ResearchMethod researchMethod : researchMethods) {
            ResearchMethodDTO researchMethodDTO = new ResearchMethodDTO();
            researchMethodDTO.setId((researchMethod.getRmid()).toString());
            researchMethodDTO.setName(researchMethod.getName());
            researchMethodDTOS.add(researchMethodDTO);
        }

        return researchMethodDTOS;
    }

}
