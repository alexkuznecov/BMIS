package by.grsu.converters;

import by.grsu.dto.ResearchObjectTypeDTO;
import by.grsu.entity.ResearchObjectType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public class ResearchObjectTypeConverter {

    public static List<ResearchObjectTypeDTO> convertToResearchObjectTypeDTO(List<ResearchObjectType> researchObjectTypes) {
        List<ResearchObjectTypeDTO> researchObjectTypeDTOS = new ArrayList<>();

        for (ResearchObjectType researchObjectType : researchObjectTypes) {
            ResearchObjectTypeDTO researchObjectTypeDTO = new ResearchObjectTypeDTO();
            researchObjectTypeDTO.setId(researchObjectType.getRotid().toString());
            researchObjectTypeDTO.setName(researchObjectType.getName());
            researchObjectTypeDTOS.add(researchObjectTypeDTO);
        }

        return researchObjectTypeDTOS;
    }

}
