package by.grsu.converters;

import by.grsu.dto.ResearchPassportDTO;
import by.grsu.entity.ResearchPassport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 13.4.17.
 */
public class ResearchPassportConverter {

    public static List<ResearchPassportDTO> convertToResearchPassportDTO(List<ResearchPassport> researchPassportList) {
        List<ResearchPassportDTO> researchPassportDTOS = new ArrayList<>();

        for (ResearchPassport researchPassport : researchPassportList) {
            ResearchPassportDTO researchPassportDTO = new ResearchPassportDTO();
            researchPassportDTO.setId(researchPassport.getRpid().toString());
            researchPassportDTO.setName(researchPassport.getDescription());
            researchPassportDTOS.add(researchPassportDTO);
        }

        return researchPassportDTOS;
    }

}
