package by.grsu.converters;

import by.grsu.dto.SpectrLineDTO;
import by.grsu.entity.SpectrLine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 13.4.17.
 */
public class SpectrLineConverter {

    public static List<SpectrLineDTO> convertToSpectrLineDTO(List<SpectrLine> spectrLines) {
        List<SpectrLineDTO> spectrLineDTOS = new ArrayList<>();

        for (SpectrLine spectrLine : spectrLines) {
            SpectrLineDTO spectrLineDTO = new SpectrLineDTO();
            spectrLineDTO.setId(spectrLine.getSlid().toString());
            spectrLineDTO.setName(spectrLine.getPersonName());
            spectrLineDTOS.add(spectrLineDTO);
        }

        return spectrLineDTOS;
    }

}
