package by.grsu.converters;

import by.grsu.dto.UserDTO;
import by.grsu.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
public class UserConverter {

    public static List<UserDTO> convertToUserDTO(List<User> users) {
        List<UserDTO> userDTOS = new ArrayList<>();

        for (User user : users) {
            UserDTO userDTO = new UserDTO();
            userDTO.setLogin(user.getLogin());
            userDTO.setEmail(user.getEmail());
            userDTO.setFirstName(user.getName());
            userDTO.setLastName(user.getSurname());
            userDTOS.add(userDTO);
        }

        return userDTOS;
    }

    public static UserDTO convertToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin(user.getLogin());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getName());
        userDTO.setLastName(user.getSurname());

        return userDTO;
    }

}
