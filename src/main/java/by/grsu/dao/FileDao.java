package by.grsu.dao;

import by.grsu.entity.File;

import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
public interface FileDao {

    void saveFile(File file);

    List<File> findAllFiles();

    void deleteFileById(Integer id);

    File findById(Integer id);

    void updateFile(File file);

    File getByUniqueFileName(String name);

    List<File> getByType(String type);
}
