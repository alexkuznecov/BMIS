package by.grsu.dao;

import by.grsu.entity.Credentials;
import by.grsu.entity.User;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public interface UserDao {

    void save(User user);

    void saveCredentials(Credentials credentials);

    List<User> getAll();

    Integer check(String login, String password);

    String checkUid(Integer id);

    User getById(Integer id);

    void insertUid(Integer id, String uuid);

    User getByLogin(String login);

    void deleteUid(String uuid);

    Boolean updatePassword(String login, String newPassword);

    void updateUser(User user);

}
