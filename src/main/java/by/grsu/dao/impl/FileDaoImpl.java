package by.grsu.dao.impl;

import by.grsu.dao.AbstractDao;
import by.grsu.dao.FileDao;
import by.grsu.entity.File;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
@Repository("fileDao")
public class FileDaoImpl extends AbstractDao implements FileDao {

    @Override
    public void saveFile(File file) {
        persist(file);
    }

    @Override
    public File getByUniqueFileName(String name) {
        Criteria criteria = getSession().createCriteria(File.class);
        criteria.add(Restrictions.eq("name", name));
        return (File) criteria.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<File> findAllFiles() {
        Criteria criteria = getSession().createCriteria(File.class);
        return (List<File>) criteria.list();
    }

    @Override
    public void deleteFileById(Integer id) {
        Query query = getSession().createSQLQuery("delete from File where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    public File findById(Integer id) {
        Criteria criteria = getSession().createCriteria(File.class);
        criteria.add(Restrictions.eq("id", id));
        return (File) criteria.uniqueResult();
    }

    @Override
    public void updateFile(File file) {
        getSession().update(file);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<File> getByType(String type) {
        Criteria criteria = getSession().createCriteria(File.class);
        criteria.add(Restrictions.eq("type", type));
        return criteria.list();
    }
}
