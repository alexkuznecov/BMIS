package by.grsu.dao.impl;

import by.grsu.dao.AbstractDao;
import by.grsu.dao.UserDao;
import by.grsu.entity.Credentials;
import by.grsu.entity.User;
import by.grsu.entity.Uuid;
import by.grsu.service.impl.UserServiceImpl;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by alek on 6.4.17.
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public void save(User user) {
        persist(user);
    }

    @Override
    public void saveCredentials(Credentials credentials) {
        persist(credentials);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAll() {
        Criteria criteria = getSession().createCriteria(User.class);
        return (List<User>) criteria.list();
    }

    @Override
    public Integer check(String login, String password) {
        Criteria criteria = getSession().createCriteria(Credentials.class);
        criteria.add(Restrictions.eq("login",login));
        criteria.add(Restrictions.eq("password", password));
        if (criteria.uniqueResult() != null) {
            Criteria newCriteria = getSession().createCriteria(User.class);
            newCriteria.add(Restrictions.eq("login", ((Credentials) criteria.uniqueResult()).getLogin()));
            return ((User) newCriteria.uniqueResult()).getId();
        } else {
            return -1;
        }
    }

    @Override
    public User getById(Integer id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id",id));
        return (User) criteria.uniqueResult();
    }

    @Override
    public void insertUid(Integer id, String userUuid) {
        Uuid uuid = new Uuid();
        uuid.setUserId(id);
        uuid.setUuid(userUuid);
        persist(uuid);
    }

    @Override
    public User getByLogin(String login) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("login", login));
        return (User) criteria.uniqueResult();
    }

    @Override
    public String checkUid(Integer id) {
        Criteria criteria = getSession().createCriteria(Uuid.class);
        criteria.add(Restrictions.eq("userId", id));
        return criteria.uniqueResult() != null ? ((Uuid) criteria.uniqueResult()).getUuid() : "";
    }

    @Override
    public void deleteUid(String uuid) {
        Query query = getSession().createSQLQuery("delete from Uuid where uuid = :uuid");
        query.setString("uuid", uuid);
        query.executeUpdate();
    }

    @Override
    public Boolean updatePassword(String login, String newPassword) {
        try {
            Criteria criteria = getSession().createCriteria(Credentials.class);
            criteria.add(Restrictions.eq("login", login));
            Credentials credentials = (Credentials) criteria.uniqueResult();

            credentials.setPassword(newPassword);

            getSession().update(credentials);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public void updateUser(User user) {
        getSession().update(user);
    }
}
