package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by alek on 19.3.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BuildingMaterialsDTO {

    private String id;

    //for table in form
    private String name;

    private String creationDate;

    private String manufacturerName;

    private String researchObjectTypeName;

    //for saving

    private String mark;

    private String shortName;

    private String madeYear;

    private String frostResistance;

    private String mechanicalStress;

    private String spectrFile;

    private List<String> materials;

    private List<String> qualityStandarts;

    private String manufacturerId;

    private String researchObjectTypeId;

    public BuildingMaterialsDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getResearchObjectTypeName() {
        return researchObjectTypeName;
    }

    public void setResearchObjectTypeName(String researchObjectTypeName) {
        this.researchObjectTypeName = researchObjectTypeName;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMadeYear() {
        return madeYear;
    }

    public void setMadeYear(String madeYear) {
        this.madeYear = madeYear;
    }

    public String getFrostResistance() {
        return frostResistance;
    }

    public void setFrostResistance(String frostResistance) {
        this.frostResistance = frostResistance;
    }

    public String getMechanicalStress() {
        return mechanicalStress;
    }

    public void setMechanicalStress(String mechanicalStress) {
        this.mechanicalStress = mechanicalStress;
    }

    public String getSpectrFile() {
        return spectrFile;
    }

    public void setSpectrFile(String spectrFile) {
        this.spectrFile = spectrFile;
    }

    public List<String> getMaterials() {
        return materials;
    }

    public void setMaterials(List<String> materials) {
        this.materials = materials;
    }

    public List<String> getQualityStandarts() {
        return qualityStandarts;
    }

    public void setQualityStandarts(List<String> qualityStandarts) {
        this.qualityStandarts = qualityStandarts;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getResearchObjectTypeId() {
        return researchObjectTypeId;
    }

    public void setResearchObjectTypeId(String researchObjectTypeId) {
        this.researchObjectTypeId = researchObjectTypeId;
    }
}
