package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 19.3.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EthalonSpectrDTO {

    private String id;

    //for tables

    private String waveLength;

    private String buildMaterialName;

    private String materialName;

    private String chemicalElementName;

    private String spectrLinePersonName;

    //for saving

    private String material;

    private String buildMaterial;

    private String chemicalElement;

    private String spectrLine;

    private String lineDescription;

    private String spectrBase;

    public EthalonSpectrDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWaveLength() {
        return waveLength;
    }

    public void setWaveLength(String waveLength) {
        this.waveLength = waveLength;
    }

    public String getBuildMaterialName() {
        return buildMaterialName;
    }

    public void setBuildMaterialName(String buildMaterialName) {
        this.buildMaterialName = buildMaterialName;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getChemicalElementName() {
        return chemicalElementName;
    }

    public void setChemicalElementName(String chemicalElementName) {
        this.chemicalElementName = chemicalElementName;
    }

    public String getSpectrLinePersonName() {
        return spectrLinePersonName;
    }

    public void setSpectrLinePersonName(String spectrLinePersonName) {
        this.spectrLinePersonName = spectrLinePersonName;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getBuildMaterial() {
        return buildMaterial;
    }

    public void setBuildMaterial(String buildMaterial) {
        this.buildMaterial = buildMaterial;
    }

    public String getChemicalElement() {
        return chemicalElement;
    }

    public void setChemicalElement(String chemicalElement) {
        this.chemicalElement = chemicalElement;
    }

    public String getSpectrLine() {
        return spectrLine;
    }

    public void setSpectrLine(String spectrLine) {
        this.spectrLine = spectrLine;
    }

    public String getLineDescription() {
        return lineDescription;
    }

    public void setLineDescription(String lineDescription) {
        this.lineDescription = lineDescription;
    }

    public String getSpectrBase() {
        return spectrBase;
    }

    public void setSpectrBase(String spectrBase) {
        this.spectrBase = spectrBase;
    }
}
