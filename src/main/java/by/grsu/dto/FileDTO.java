package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 1.5.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileDTO {

    private String id;

    private String name;

    private String directUrl;

    public FileDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirectUrl() {
        return directUrl;
    }

    public void setDirectUrl(String directUrl) {
        this.directUrl = directUrl;
    }
}
