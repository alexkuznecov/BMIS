package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 8.3.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MaterialDTO {

    private String id;

    private String name;

    private String probeDate;

    private String probePlace;

    private String description;

    //for saving

    private String spectrFile;

    public MaterialDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProbeDate() {
        return probeDate;
    }

    public void setProbeDate(String probeDate) {
        this.probeDate = probeDate;
    }

    public String getProbePlace() {
        return probePlace;
    }

    public void setProbePlace(String probePlace) {
        this.probePlace = probePlace;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpectrFile() {
        return spectrFile;
    }

    public void setSpectrFile(String spectrFile) {
        this.spectrFile = spectrFile;
    }
}
