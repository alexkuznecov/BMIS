package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 14.4.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationDTO {

    private String id;

    private String name;

    //for saving

    private String fullTitle;

    private String shortTitle;

    public OrganizationDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullTitle() {
        return fullTitle;
    }

    public void setFullTitle(String fullTitle) {
        this.fullTitle = fullTitle;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }
}
