package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 6.4.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QualityStandartDTO {

    private String id;

    private String name;

    public QualityStandartDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
