package by.grsu.dto;

import by.grsu.util.StringCrypter;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 18.4.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegistrationDataDTO {

    private String loginName;

    private String password;

    private String name;

    private String lastName;

    private String email;

    public RegistrationDataDTO() {

    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
