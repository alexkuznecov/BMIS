package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 1.5.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResearchMethodDTO {

    private String id;

    //for save

    private String name;

    private String description;

    public ResearchMethodDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
