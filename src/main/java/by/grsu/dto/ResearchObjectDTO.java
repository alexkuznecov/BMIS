package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 19.3.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResearchObjectDTO {

    private String id;

    private String name;

    private String organizationName;

    private String date;

    private String description;

    //for save

    private String organization;

    public ResearchObjectDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
