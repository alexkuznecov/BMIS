package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 13.4.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResearchPassportDTO {

    private String id;

    private String name;

    //for save

    private String researchMethod;

    private String researchObject;

    private String fileId;

    private String description;

    private String intensity;

    public ResearchPassportDTO () {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResearchMethod() {
        return researchMethod;
    }

    public void setResearchMethod(String researchMethod) {
        this.researchMethod = researchMethod;
    }

    public String getResearchObject() {
        return researchObject;
    }

    public void setResearchObject(String researchObject) {
        this.researchObject = researchObject;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }
}
