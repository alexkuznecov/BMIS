package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 19.3.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpectrDTO {

    private String id;

    private String waveLength;

    private String intensity;

    private String chemicalElementName;

    private String spectrLineName;

    //for saving

    private String researchPassport;

    private String spectrLine;

    private String chemicalElement;

    private String lineDescription;

    private String spectrBase;

    public SpectrDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWaveLength() {
        return waveLength;
    }

    public void setWaveLength(String waveLength) {
        this.waveLength = waveLength;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getChemicalElementName() {
        return chemicalElementName;
    }

    public void setChemicalElementName(String chemicalElementName) {
        this.chemicalElementName = chemicalElementName;
    }

    public String getSpectrLineName() {
        return spectrLineName;
    }

    public void setSpectrLineName(String spectrLineName) {
        this.spectrLineName = spectrLineName;
    }

    public String getResearchPassport() {
        return researchPassport;
    }

    public void setResearchPassport(String researchPassport) {
        this.researchPassport = researchPassport;
    }

    public String getSpectrLine() {
        return spectrLine;
    }

    public void setSpectrLine(String spectrLine) {
        this.spectrLine = spectrLine;
    }

    public String getChemicalElement() {
        return chemicalElement;
    }

    public void setChemicalElement(String chemicalElement) {
        this.chemicalElement = chemicalElement;
    }

    public String getLineDescription() {
        return lineDescription;
    }

    public void setLineDescription(String lineDescription) {
        this.lineDescription = lineDescription;
    }

    public String getSpectrBase() {
        return spectrBase;
    }

    public void setSpectrBase(String spectrBase) {
        this.spectrBase = spectrBase;
    }
}
