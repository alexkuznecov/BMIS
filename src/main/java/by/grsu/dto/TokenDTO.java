package by.grsu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by alek on 10.4.17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenDTO {

    private Boolean success;

    private String authToken;

    public TokenDTO() {

    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
