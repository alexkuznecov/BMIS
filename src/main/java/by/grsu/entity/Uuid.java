package by.grsu.entity;

import javax.persistence.*;

/**
 * Created by alek on 6.4.17.
 */
@Entity
@Table(name = "Uuid")
public class Uuid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 6, nullable = false)
    private Integer id;

    @Column(name = "userId")
    private Integer userId;

    @Column(name = "uuid")
    private String uuid;

    public Uuid() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
