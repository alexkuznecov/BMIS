package by.grsu.enums;

/**
 * Created by alek on 1.5.17.
 */
public enum FileType {

    ATTACHMENT,
    RESEARCH_PASSPORT,
    SPECTR,
    ATTACHMENT_IMG

}
