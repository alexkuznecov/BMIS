package by.grsu.enums;

/**
 * Created by alek on 9.3.17.
 */
public enum Filter {

    NAME,
    PROBEDATE,
    PROBEPLACE,
    DESCRIPTION,
    WAVELENGTH,
    BUILDMATERIALNAME,
    CHEMICALELEMENTNAME,
    SPECTRLINEPERSONNAME,
    CREATIONDATE,
    MANUFACTURERNAME,
    RESEARCHOBJECTTYPENAME,
    ORGANIZATIONNAME,
    DATE,
    INTENSITY,
    SPECTRLINENAME,
    MATERIALNAME

}
