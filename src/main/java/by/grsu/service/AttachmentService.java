package by.grsu.service;

import by.grsu.dto.AttachmentDTO;

/**
 * Created by alek on 1.5.17.
 */
public interface AttachmentService {

    Boolean add(AttachmentDTO attachmentDTO);

}
