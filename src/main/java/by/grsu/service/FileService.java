package by.grsu.service;

import by.grsu.dto.FileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by alek on 1.5.17.
 */
public interface FileService {

    Boolean saveFileInfo(String name, String alias, String type, String directUrl);

    List<FileDTO> getIdAndName(String type);

    Boolean saveFile(MultipartFile file, String fileName, String typeOfFile) throws IOException;

    FileDTO saveImage(MultipartFile file, String typeOfFile);

    String resolveFileType(String fileType);

    String getImage(String id);
}
