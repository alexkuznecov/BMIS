package by.grsu.service;

import by.grsu.dto.ManufacturersDTO;
import by.grsu.entity.Manufacturer;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public interface ManufacturerService {

    List<ManufacturersDTO> getManufactorersIdName();

    Boolean add(ManufacturersDTO manufacturersDTO);

}
