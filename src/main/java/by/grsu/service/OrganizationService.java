package by.grsu.service;

import by.grsu.dto.OrganizationDTO;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
public interface OrganizationService {

    List<OrganizationDTO> getOrganizationsIdName();

    Boolean add(OrganizationDTO organizationDTO);
}
