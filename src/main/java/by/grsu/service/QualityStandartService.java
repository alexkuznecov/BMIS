package by.grsu.service;

import by.grsu.dto.QualityStandartDTO;
import by.grsu.entity.QualityStandart;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public interface QualityStandartService {

    List<QualityStandartDTO> getQualityStandartsIdName();

    Boolean add(QualityStandartDTO qualityStandartDTO);

}
