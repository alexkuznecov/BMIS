package by.grsu.service;

import by.grsu.dto.ResearchMethodDTO;
import by.grsu.entity.ResearchMethod;

import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
public interface ResearchMethodService {

    Boolean add(ResearchMethodDTO researchMethodDTO);

    List<ResearchMethodDTO> getIdAndName();

}
