package by.grsu.service;

import by.grsu.dto.ResearchObjectTypeDTO;
import by.grsu.entity.ResearchObjectType;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
public interface ResearchObjectTypeService {

    List<ResearchObjectTypeDTO> getResearchObjectTypesIdName();

    Boolean add(ResearchObjectTypeDTO researchObjectTypeDTO);

}
