package by.grsu.service;

import by.grsu.dto.ResearchPassportDTO;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
public interface ResearchPassportService {

    List<ResearchPassportDTO> getResearchPasportsIdName();

    Boolean add(ResearchPassportDTO researchPassportDTO);

}
