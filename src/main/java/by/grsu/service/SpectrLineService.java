package by.grsu.service;

import by.grsu.dto.SpectrLineDTO;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
public interface SpectrLineService {

    List<SpectrLineDTO> getSpectrLinesIdName();

}
