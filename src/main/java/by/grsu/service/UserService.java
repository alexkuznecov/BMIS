package by.grsu.service;

import by.grsu.dto.TokenDTO;
import by.grsu.dto.UserDTO;

/**
 * Created by alek on 6.4.17.
 */
public interface UserService {

    TokenDTO checkUser(String login, String password);

    void add(String name, String surname, String email, String login, String password);

    Boolean checkLogin(String login);

    Boolean logout(String uuid);

    Boolean passwordRecovery(String login);

    UserDTO getByLogin(String login);

    Boolean update(UserDTO userDTO);

    Boolean updatePassword(String login, String newPassword);
}
