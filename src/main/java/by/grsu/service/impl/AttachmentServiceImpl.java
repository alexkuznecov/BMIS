package by.grsu.service.impl;

import by.grsu.dao.AttachmentDao;
import by.grsu.dto.AttachmentDTO;
import by.grsu.entity.Attachment;
import by.grsu.service.AttachmentService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by alek on 1.5.17.
 */
@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(AttachmentServiceImpl.class);

    @Autowired
    private AttachmentDao attachmentDao;

    @Override
    public Boolean add(AttachmentDTO attachmentDTO) {
        try {
            Attachment attachment = new Attachment();
            attachment.setName(attachmentDTO.getName());
            attachment.setDescription(attachmentDTO.getDescription());
            attachment.setAttachFile(attachmentDTO.getFileId());
            attachment.setRoid(Integer.parseInt(attachmentDTO.getResearchObject()));

            attachmentDao.saveAttachment(attachment);
            return null;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
