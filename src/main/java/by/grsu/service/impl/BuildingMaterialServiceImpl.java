package by.grsu.service.impl;

import by.grsu.converters.BuildingMaterialConverter;
import by.grsu.dao.*;
import by.grsu.dto.BuildingMaterialsDTO;
import by.grsu.entity.BuildingMaterial;
import by.grsu.entity.MaterialHasElementSource;
import by.grsu.entity.MaterialHasQualityStandart;
import by.grsu.service.BuildingMaterialService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 19.3.17.
 */
@Service("buildingMaterialService")
@Transactional
public class BuildingMaterialServiceImpl implements BuildingMaterialService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(BuildingMaterialServiceImpl.class);

    @Autowired
    private ManufacturerDao manufacturerDao;

    @Autowired
    private ResearchObjectTypeDao researchObjectTypeDao;

    @Autowired
    private BuildingMaterialDao buildingMaterialDao;

    @Autowired
    private MaterialHasElementSourceDao materialHasElementSourceDao;

    @Autowired
    private MaterialHasQualityStandartDao materialHasQualityStandartDao;

    @Override
    public boolean add(BuildingMaterialsDTO buildingMaterialsDTO) {
        try {
            BuildingMaterial buildingMaterial = new BuildingMaterial();
            buildingMaterial.setFrostResistance(buildingMaterialsDTO.getFrostResistance());
            buildingMaterial.setMechanicalStress(buildingMaterialsDTO.getMechanicalStress());
            buildingMaterial.setMark(buildingMaterialsDTO.getMark());
            buildingMaterial.setShortName(buildingMaterialsDTO.getShortName());
            buildingMaterial.setSpectrFile(buildingMaterialsDTO.getSpectrFile());
            buildingMaterial.setYear(buildingMaterialsDTO.getMadeYear());
            buildingMaterial.setMnfid(Integer.parseInt(buildingMaterialsDTO.getManufacturerId()));
            buildingMaterial.setRotid(Integer.parseInt(buildingMaterialsDTO.getResearchObjectTypeId()));

            Integer bmid = buildingMaterialDao.saveBuildingMaterial(buildingMaterial);

            for (String qsid : buildingMaterialsDTO.getQualityStandarts()) {
                MaterialHasQualityStandart materialHasQualityStandart = new MaterialHasQualityStandart();
                materialHasQualityStandart.setBmid(bmid);
                materialHasQualityStandart.setQsid(Integer.parseInt(qsid));
                materialHasQualityStandartDao.saveMaterialHasQualityStandart(materialHasQualityStandart);
            }

            for (String mid : buildingMaterialsDTO.getMaterials()) {
                MaterialHasElementSource materialHasElementSource = new MaterialHasElementSource();
                materialHasElementSource.setBmid(bmid);
                materialHasElementSource.setMid(Integer.parseInt(mid));
                materialHasElementSourceDao.saveMaterialHasElementSource(materialHasElementSource);
            }
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
        return true;
    }

    @Override
    public List<BuildingMaterialsDTO> getByFilter(String name, String creationDate, String manufacturerName, String researchObjectTypeName, Integer paramCount) {

        try {
            List<Integer> manufacturerId = new ArrayList<>();
            List<Integer> researchObjectId = new ArrayList<>();

            if (!manufacturerName.equals("")) {
                manufacturerId = manufacturerDao.getIdByName(manufacturerName);
            }
            if (!researchObjectTypeName.equals("")) {
                researchObjectId = researchObjectTypeDao.getIdByName(researchObjectTypeName);
            }

            return BuildingMaterialConverter.convertToBuildingMaterialResponse(buildingMaterialDao.getByVariableParameters(name, creationDate, manufacturerId, researchObjectId, paramCount),
                    manufacturerDao, researchObjectTypeDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<BuildingMaterialsDTO> getAllBuildingMaterials() {
        try {
            return BuildingMaterialConverter.convertToBuildingMaterialResponse(buildingMaterialDao.findAllBuildingMaterials(), manufacturerDao, researchObjectTypeDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<BuildingMaterialsDTO> getBuildingMaterialsIdName() {
        try {
            return BuildingMaterialConverter.convertToBuildingMaterialDTO(buildingMaterialDao.findAllBuildingMaterials());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}
