package by.grsu.service.impl;

import by.grsu.converters.ChemicalElementConverter;
import by.grsu.dao.ChemicalElementDao;
import by.grsu.dto.ChemicalElementsDTO;
import by.grsu.entity.ChemicalElement;
import by.grsu.service.ChemicalElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 15.2.17.
 */
@Service("chemicalElementService")
@Transactional
public class ChemicalElementServiceImpl implements ChemicalElementService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ChemicalElementServiceImpl.class);

    @Autowired
    private ChemicalElementDao dao;

    public void saveChemicalElement(ChemicalElement chemicalElement) {
        try {
            dao.saveChemicalElement(chemicalElement);
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    @Override
    public void saveChemicalElements(List<ChemicalElement> chemicalElements) {
        try {
            for (int i = 0; i < chemicalElements.size(); i++) {
                dao.saveChemicalElement(chemicalElements.get(i));
            }
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    public List<ChemicalElement> findAllChemicalElement() {
        try {
            return dao.findAllChemicalElement();
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    public void deleteChemicalElementById(Integer id) {
        try {
            dao.deleteChemicalElementById(id);
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    public ChemicalElement findById(Integer id) {
        try {
            return dao.findById(id);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    public void updateChemicalElement(ChemicalElement chemicalElement){
        try {
            dao.updateChemicalElement(chemicalElement);
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    public String getAll() {
        try {
            return dao.getAll();
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<ChemicalElementsDTO> getChemicalElementsIdName() {
        try {
            return ChemicalElementConverter.convertToChemicalElementsDTO(dao.findAllChemicalElement());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}
