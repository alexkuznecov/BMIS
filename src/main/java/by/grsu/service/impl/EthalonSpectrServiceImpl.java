package by.grsu.service.impl;

import by.grsu.converters.EthalonSpectrConverter;
import by.grsu.dao.*;
import by.grsu.dto.EthalonSpectrDTO;
import by.grsu.entity.EthalonSpectr;
import by.grsu.service.EthalonSpectrService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alek on 19.3.17.
 */
@Service("ethalonSpectrService")
@Transactional
public class EthalonSpectrServiceImpl implements EthalonSpectrService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(EthalonSpectrServiceImpl.class);

    @Autowired
    private EthalonSpectrDao ethalonSpectrDao;

    @Autowired
    private BuildingMaterialDao buildingMaterialDao;

    @Autowired
    private MaterialDao materialDao;

    @Autowired
    private ChemicalElementDao chemicalElementDao;

    @Autowired
    private SpectrLineDao spectrLineDao;

    @Autowired
    private FileDao fileDao;

    @Override
    public List<EthalonSpectrDTO> getByFilter(String waveLength, String buildMaterialName, String materialName, String chemicalElementName, String spectrLinePersonName, Integer paramCount) {

        try {
            List<Integer> buildMaterialId = new ArrayList<>();
            List<Integer> materialId = new ArrayList<>();
            List<Integer> chemicalElementId = new ArrayList<>();
            List<Integer> spectrLineId = new ArrayList<>();

            if (!buildMaterialName.equals("")) {
                buildMaterialId = buildingMaterialDao.getIdByName(buildMaterialName);
            }
            if (!materialName.equals("")) {
                materialId = materialDao.getIdByName(materialName);
            }
            if (!chemicalElementName.equals("")) {
                chemicalElementId = chemicalElementDao.getIdByName(chemicalElementName);
            }
            if (!spectrLinePersonName.equals("")) {
                spectrLineId = spectrLineDao.getIdByName(spectrLinePersonName);
            }

            return EthalonSpectrConverter.convertToEthalonSpectrResponse(ethalonSpectrDao.getByVariableParameters(waveLength, buildMaterialId, materialId, chemicalElementId, spectrLineId, paramCount),
                    buildingMaterialDao, materialDao, chemicalElementDao, spectrLineDao, fileDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<EthalonSpectrDTO> getAllEthalonSpectrs() {
        try {
            return EthalonSpectrConverter.convertToEthalonSpectrResponse(ethalonSpectrDao.findAllEthalonSpectrs(),
                    buildingMaterialDao, materialDao, chemicalElementDao, spectrLineDao, fileDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public boolean add(EthalonSpectrDTO ethalonSpectrDTO) {
        try {
            EthalonSpectr ethalonSpectr = new EthalonSpectr();

            ethalonSpectr.setMid(Integer.parseInt(ethalonSpectrDTO.getMaterial()));
            ethalonSpectr.setBmid(Integer.parseInt(ethalonSpectrDTO.getBuildMaterial()));
            ethalonSpectr.setCeid(Integer.parseInt(ethalonSpectrDTO.getChemicalElement()));
            ethalonSpectr.setSlid(Integer.parseInt(ethalonSpectrDTO.getSpectrLine()));
            ethalonSpectr.setWaveLength(ethalonSpectrDTO.getWaveLength());
            ethalonSpectr.setLineDescription(ethalonSpectrDTO.getLineDescription());
            ethalonSpectr.setSpectrBase(ethalonSpectrDTO.getSpectrBase());

            ethalonSpectrDao.saveEthalonSpectr(ethalonSpectr);

            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }

    }
}
