package by.grsu.service.impl;

import by.grsu.converters.FileConverter;
import by.grsu.dao.FileDao;
import by.grsu.dto.FileDTO;
import by.grsu.entity.File;
import by.grsu.enums.FileType;
import by.grsu.service.FileService;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by alek on 1.5.17.
 */
@Service("fileService")
@Transactional
public class FileServiceImpl implements FileService{

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(EthalonSpectrServiceImpl.class);

    private static String ACCESS_TOKEN = "k1XQ6z3l5nAAAAAAAAAABpiAbUYl5aYve3zX6H77DzUXPRfStD-829B1IvSLZwlQ";

    @Autowired
    private FileDao fileDao;

    @Override
    public Boolean saveFileInfo(String name, String alias, String type, String directUrl) {
        try {
            File file = new File();
            file.setName(name);
            file.setAlias(alias);
            file.setType(type);
            if (!directUrl.equals("")) {
                file.setDirectUrl(directUrl);
            }

            fileDao.saveFile(file);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public List<FileDTO> getIdAndName(String type) {
        try {
            return FileConverter.convertToFileDTO(fileDao.getByType(type));
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public String resolveFileType(String fileType) {
        String typeOfFile = "";
        if (fileType.toUpperCase().equals(FileType.RESEARCH_PASSPORT.name())) {
            typeOfFile = FileType.RESEARCH_PASSPORT.name();
        } else {
            if (fileType.toUpperCase().equals(FileType.SPECTR.name())) {
                typeOfFile = FileType.SPECTR.name();
            } else if (fileType.toUpperCase().equals(FileType.ATTACHMENT.name())) {
                typeOfFile = FileType.ATTACHMENT.name();
            } else {
                typeOfFile = FileType.ATTACHMENT_IMG.name();
            }
        }
        return typeOfFile;
    }

    @Override
    public Boolean saveFile(MultipartFile file, String fileName, String typeOfFile) throws IOException {
        String originalFileName = file.getOriginalFilename();
        String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
        UUID uuid = UUID.randomUUID();
        String name = uuid.toString() + fileExtension;
        String type = resolveFileType(typeOfFile);

        //TODO:Try to replace configuration into bean
        DbxRequestConfig config = new DbxRequestConfig("BMSS");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
        try (InputStream in = file.getInputStream()) {
            FileMetadata metadata = client.files().uploadBuilder("/" + name)
                 .uploadAndFinish(in);
            log.debug(metadata.toString());
            saveFileInfo(name, fileName, type, "");
            log.debug("info saved");
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public FileDTO saveImage(MultipartFile file, String typeOfFile) {
        String originalFileName = file.getOriginalFilename();
        String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
        UUID uuid = UUID.randomUUID();
        String name = uuid.toString() + fileExtension;
        String type = resolveFileType(typeOfFile);
        FileDTO fileDTO = new FileDTO();

        //TODO:Try to replace configuration into bean
        DbxRequestConfig config = new DbxRequestConfig("BMSS");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
        try (InputStream in = file.getInputStream()) {
            FileMetadata metadata = client.files().uploadBuilder("/" + name)
                    .uploadAndFinish(in);
            log.debug(metadata.toString());
            SharedLinkMetadata sharedLinkMetadata  = client.sharing().createSharedLinkWithSettings("/" + name);
            String directUtl = sharedLinkMetadata.getUrl().replaceFirst("www.dropbox.com", "dl.dropboxusercontent.com");
            saveFileInfo(name, originalFileName, type, directUtl);
            log.debug("info saved");
            fileDTO.setId(fileDao.getByUniqueFileName(name).getId().toString());
            fileDTO.setDirectUrl(directUtl);
            log.debug(directUtl);
            return fileDTO;
        } catch (Exception e) {
            log.error(e.toString());
            fileDTO.setId("error");
            return fileDTO;
        }
    }

    @Override
    public String getImage(String id) {
        return fileDao.findById(Integer.parseInt(id)).getDirectUrl();
    }
}
