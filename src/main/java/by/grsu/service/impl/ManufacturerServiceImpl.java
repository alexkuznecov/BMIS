package by.grsu.service.impl;

import by.grsu.converters.ManufacturerConverter;
import by.grsu.converters.MaterialConverter;
import by.grsu.dao.ManufacturerDao;
import by.grsu.dao.MaterialDao;
import by.grsu.dto.ManufacturersDTO;
import by.grsu.entity.Manufacturer;
import by.grsu.service.ManufacturerService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
@Service("manufacturerService")
@Transactional
public class ManufacturerServiceImpl implements ManufacturerService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ManufacturerServiceImpl.class);

    @Autowired
    private ManufacturerDao manufacturerDao;

    @Override
    public List<ManufacturersDTO> getManufactorersIdName() {
        try {
            return ManufacturerConverter.convertToManufactorerDTO(manufacturerDao.findAllManufacturers());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public Boolean add(ManufacturersDTO manufacturersDTO) {
        try {
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.setName(manufacturersDTO.getName());
            manufacturer.setDescription(manufacturersDTO.getDescription());
            manufacturer.setYearOfCreation(manufacturersDTO.getYear());

            manufacturerDao.saveManufacturer(manufacturer);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
