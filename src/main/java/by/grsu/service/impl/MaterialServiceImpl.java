package by.grsu.service.impl;

import by.grsu.converters.MaterialConverter;
import by.grsu.dao.MaterialDao;
import by.grsu.entity.Material;
import by.grsu.dto.MaterialDTO;
import by.grsu.service.MaterialService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by alek on 8.3.17.
 */
@Service("materialService")
@Transactional
public class MaterialServiceImpl implements MaterialService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(MaterialServiceImpl.class);

    @Autowired
    private MaterialDao materialDao;

    @Override
    public List<MaterialDTO> getByFilter(String name, String probDate, String probPlace, String description, Integer paramCount) {
        try {
            List<Material> materials = materialDao.getByVariableParameters(name, probDate, probPlace, description, paramCount);
            return MaterialConverter.convertToMaterialResponse(materials);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    public List<MaterialDTO> getAllMaterials() {
        try {
            return MaterialConverter.convertToMaterialResponse(materialDao.findAllMaterials());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<MaterialDTO> getMaterialsIdName() {
        try {
            return MaterialConverter.convertToMaterialDTO(materialDao.findAllMaterials());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public boolean add(MaterialDTO materialDTO) {
        try {
            Material material = new Material();
            material.setName(materialDTO.getName());

            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            dateFormat.setLenient(false);
            material.setProbeDate(dateFormat.format(dateFormat.parse(materialDTO.getProbeDate())));

            material.setProbePlace(materialDTO.getProbePlace());
            material.setDescription(materialDTO.getDescription());
            material.setSpectrFile(materialDTO.getSpectrFile());

            materialDao.saveMaterial(material);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
