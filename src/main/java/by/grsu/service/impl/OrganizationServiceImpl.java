package by.grsu.service.impl;

import by.grsu.converters.OrganizationConverter;
import by.grsu.dao.OrganizationDao;
import by.grsu.dto.OrganizationDTO;
import by.grsu.entity.Organization;
import by.grsu.service.OrganizationService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
@Service("organizationService")
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(OrganizationServiceImpl.class);

    @Autowired
    private OrganizationDao organizationDao;

    @Override
    public List<OrganizationDTO> getOrganizationsIdName() {
        try {
            return OrganizationConverter.convertToOrganizationDTO(organizationDao.findAllOrganizations());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public Boolean add(OrganizationDTO organizationDTO) {
        try {
            Organization organization = new Organization();
            organization.setFullTitle(organizationDTO.getFullTitle());
            organization.setShortTitle(organizationDTO.getShortTitle());

            organizationDao.saveOrganization(organization);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
