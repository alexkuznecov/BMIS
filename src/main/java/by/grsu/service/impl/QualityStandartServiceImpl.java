package by.grsu.service.impl;

import by.grsu.converters.QualityStandartConverter;
import by.grsu.dao.QualityStandartDao;
import by.grsu.dto.QualityStandartDTO;
import by.grsu.entity.QualityStandart;
import by.grsu.service.QualityStandartService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
@Service("qualityStandartService")
@Transactional
public class QualityStandartServiceImpl implements QualityStandartService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(QualityStandartServiceImpl.class);

    @Autowired
    private QualityStandartDao qualityStandartDao;

    @Override
    public List<QualityStandartDTO> getQualityStandartsIdName() {
        try {
            return QualityStandartConverter.convertToQualityStandartDTO(qualityStandartDao.findAllQualityStandarts());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public Boolean add(QualityStandartDTO qualityStandartDTO) {
        try {
            QualityStandart qualityStandart = new QualityStandart();
            qualityStandart.setName(qualityStandartDTO.getName());

            qualityStandartDao.saveQualityStandart(qualityStandart);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
