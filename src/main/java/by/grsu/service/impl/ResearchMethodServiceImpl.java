package by.grsu.service.impl;

import by.grsu.converters.ResearchMethodConverter;
import by.grsu.dao.ResearchMethodDao;
import by.grsu.dto.ResearchMethodDTO;
import by.grsu.entity.ResearchMethod;
import by.grsu.service.ResearchMethodService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 1.5.17.
 */
@Service("researchMethodService")
@Transactional
public class ResearchMethodServiceImpl implements ResearchMethodService {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(ResearchMethodServiceImpl.class);

    @Autowired
    private ResearchMethodDao researchMethodDao;

    @Override
    public Boolean add(ResearchMethodDTO researchMethodDTO) {
        try {
            ResearchMethod researchMethod = new ResearchMethod();
            researchMethod.setName(researchMethodDTO.getName());
            researchMethod.setDescription(researchMethodDTO.getDescription());

            researchMethodDao.saveResearchMethod(researchMethod);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public List<ResearchMethodDTO> getIdAndName() {
        try {
            return ResearchMethodConverter.convertToResearchMethodDTO(researchMethodDao.findAllResearchMethods());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}
