package by.grsu.service.impl;

import by.grsu.converters.ResearchObjectConverter;
import by.grsu.dao.OrganizationDao;
import by.grsu.dao.ResearchObjectDao;
import by.grsu.dto.ResearchObjectDTO;
import by.grsu.entity.ResearchObject;
import by.grsu.service.ResearchObjectService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alek on 19.3.17.
 */
@Service("researchObjectService")
@Transactional
public class ResearchObjectServiceImpl implements ResearchObjectService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ResearchObjectServiceImpl.class);

    @Autowired
    private ResearchObjectDao researchObjectDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Override
    public List<ResearchObjectDTO> getByFilter(String name, String organizationName, String date, String description, Integer paramCount) {

        try {
            List<Integer> organizationId = new ArrayList<>();

            if (!organizationName.equals("")) {
                organizationId = organizationDao.getIdByName(organizationName);
            }

            return ResearchObjectConverter.convertToResearchObjectResponse(researchObjectDao.getByVariableParameters(name, organizationId, date, description, paramCount),
                    organizationDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<ResearchObjectDTO> getAllResearchObjects() {
        try {
            return ResearchObjectConverter.convertToResearchObjectResponse(researchObjectDao.findAllResearchObjects(), organizationDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<ResearchObjectDTO> getResearchObjectsIdName() {
        try {
            return ResearchObjectConverter.convertToResearchObjectDTO(researchObjectDao.findAllResearchObjects());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public boolean add(ResearchObjectDTO researchObjectDTO) {
        try {
            ResearchObject researchObject = new ResearchObject();
            researchObject.setOid(Integer.parseInt(researchObjectDTO.getOrganization()));
            researchObject.setName(researchObjectDTO.getName());
            researchObject.setDescription(researchObjectDTO.getDescription());

            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            dateFormat.setLenient(false);
            researchObject.setDate(dateFormat.format(dateFormat.parse(researchObjectDTO.getDate())));

            researchObjectDao.saveResearchObject(researchObject);

            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
