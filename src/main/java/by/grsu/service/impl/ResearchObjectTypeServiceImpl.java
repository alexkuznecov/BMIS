package by.grsu.service.impl;

import by.grsu.converters.ResearchObjectConverter;
import by.grsu.converters.ResearchObjectTypeConverter;
import by.grsu.dao.ResearchObjectTypeDao;
import by.grsu.dto.ResearchObjectTypeDTO;
import by.grsu.entity.ResearchObjectType;
import by.grsu.service.ResearchObjectTypeService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 6.4.17.
 */
@Service("researchObjectTypeService")
@Transactional
public class ResearchObjectTypeServiceImpl implements ResearchObjectTypeService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ResearchObjectTypeServiceImpl.class);

    @Autowired
    private ResearchObjectTypeDao researchObjectTypeDao;

    @Override
    public List<ResearchObjectTypeDTO> getResearchObjectTypesIdName() {
        try {
            return ResearchObjectTypeConverter.convertToResearchObjectTypeDTO(researchObjectTypeDao.findAllResearchObjectTypes());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public Boolean add(ResearchObjectTypeDTO researchObjectTypeDTO) {
        try {
            ResearchObjectType researchObjectType = new ResearchObjectType();
            researchObjectType.setName(researchObjectTypeDTO.getName());

            researchObjectTypeDao.saveResearchObjectType(researchObjectType);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
