package by.grsu.service.impl;

import by.grsu.converters.ResearchPassportConverter;
import by.grsu.dao.ResearchPassportDao;
import by.grsu.dto.ResearchPassportDTO;
import by.grsu.entity.ResearchPassport;
import by.grsu.service.ResearchPassportService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
@Service("researchPassportService")
@Transactional
public class ResearchPassportServiceImpl implements ResearchPassportService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ResearchPassportServiceImpl.class);

    @Autowired
    private ResearchPassportDao researchPassportDao;

    @Override
    public List<ResearchPassportDTO> getResearchPasportsIdName() {
        try {
            return ResearchPassportConverter.convertToResearchPassportDTO(researchPassportDao.findAllResearchPassports());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public Boolean add(ResearchPassportDTO researchPassportDTO) {
        try {
            ResearchPassport researchPassport = new ResearchPassport();
            researchPassport.setDescription(researchPassportDTO.getDescription());
            researchPassport.setIntensity(researchPassportDTO.getIntensity());
            researchPassport.setRoid(Integer.parseInt(researchPassportDTO.getResearchObject()));
            researchPassport.setRmid(Integer.parseInt(researchPassportDTO.getResearchMethod()));
            researchPassport.setSpectrFile(researchPassportDTO.getFileId());

            researchPassportDao.saveResearchPassport(researchPassport);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
