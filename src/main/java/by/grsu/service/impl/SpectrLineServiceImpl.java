package by.grsu.service.impl;

import by.grsu.converters.SpectrLineConverter;
import by.grsu.dao.SpectrLineDao;
import by.grsu.dto.SpectrLineDTO;
import by.grsu.service.SpectrLineService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by alek on 14.4.17.
 */
@Service("spectrLineService")
@Transactional
public class SpectrLineServiceImpl implements SpectrLineService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(SpectrLineServiceImpl.class);

    @Autowired
    private SpectrLineDao spectrLineDao;

    @Override
    public List<SpectrLineDTO> getSpectrLinesIdName() {
        try {
            return SpectrLineConverter.convertToSpectrLineDTO(spectrLineDao.findAllSpectrLines());
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}
