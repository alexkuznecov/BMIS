package by.grsu.service.impl;

import by.grsu.converters.SpectrConverter;
import by.grsu.dao.*;
import by.grsu.dto.SpectrDTO;
import by.grsu.entity.Spectr;
import by.grsu.service.SpectrService;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alek on 19.3.17.
 */
@Service("spectrService")
@Transactional
public class SpectrServiceImpl implements SpectrService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(SpectrServiceImpl.class);

    @Autowired
    private SpectrDao spectrDao;

    @Autowired
    private ChemicalElementDao chemicalElementDao;

    @Autowired
    private ResearchPassportDao researchPassportDao;

    @Autowired
    private SpectrLineDao spectrLineDao;

    @Autowired
    private FileDao fileDao;

    @Override
    public List<SpectrDTO> getByFilter(String waveLength, String intensity, String chemicalElementName, String spectrLineName, Integer paramCount) {

        try {

            List<Integer> chemicalElementId = new ArrayList<>();
            List<Integer> researchPassportId = new ArrayList<>();
            List<Integer> spectrLineId = new ArrayList<>();

            if (!chemicalElementName.equals("")) {
                chemicalElementId = chemicalElementDao.getIdByName(chemicalElementName);
            }
            if (!intensity.equals("")) {
                researchPassportId = researchPassportDao.getIdByIntencity(intensity);
            }
            if (!spectrLineName.equals("")) {
                spectrLineId = spectrLineDao.getIdByName(spectrLineName);
            }

            return SpectrConverter.convertToSpectrResponse(spectrDao.getByVariableParameters(waveLength, researchPassportId, chemicalElementId, spectrLineId, paramCount),
                    chemicalElementDao, researchPassportDao, spectrLineDao, fileDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public List<SpectrDTO> getAllSpectrs() {
        try {
            return SpectrConverter.convertToSpectrResponse(spectrDao.findAllSpectrs(), chemicalElementDao, researchPassportDao, spectrLineDao, fileDao);
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }

    }

    @Override
    public boolean add(SpectrDTO spectrDTO) {
        try {
            Spectr spectr = new Spectr();
            spectr.setRpid(Integer.parseInt(spectrDTO.getResearchPassport()));
            spectr.setSlid(Integer.parseInt(spectrDTO.getSpectrLine()));
            spectr.setCeid(Integer.parseInt(spectrDTO.getChemicalElement()));
            spectr.setWaveLength(spectrDTO.getWaveLength());
            spectr.setLineDescription(spectrDTO.getLineDescription());
            spectr.setSpectrBase(spectrDTO.getSpectrBase());

            spectrDao.saveSpectr(spectr);

            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
