package by.grsu.service.impl;

import by.grsu.controller.UserController;
import by.grsu.converters.UserConverter;
import by.grsu.dao.UserDao;
import by.grsu.dto.TokenDTO;
import by.grsu.dto.UserDTO;
import by.grsu.entity.Credentials;
import by.grsu.entity.User;
import by.grsu.service.UserService;
import by.grsu.util.EmailSender;
import by.grsu.util.interceptor.AuthorizeInterceptor;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by alek on 6.4.17.
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Override
    public TokenDTO checkUser(String login, String password) {
        try {

            TokenDTO token = new TokenDTO();
            Integer userId = userDao.check(login, password);

            if (userId != -1) {
                String uuid = userDao.checkUid(userId);
                if (uuid.equals("")) {
                    uuid = UUID.randomUUID().toString();
                    userDao.insertUid(userId, uuid);
                }
                token.setSuccess(true);
                token.setAuthToken(uuid);
            } else {
                token.setSuccess(false);
            }
            return token;
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public void add(String name, String surname, String email, String login, String password) {
        try {
            User user = new User();
            user.setName(name);
            user.setSurname(surname);
            user.setLogin(login);
            user.setEmail(email);

            userDao.save(user);

            Credentials credentials = new Credentials();
            credentials.setLogin(login);
            credentials.setPassword(password);

            userDao.saveCredentials(credentials);
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    @Override
    public Boolean checkLogin(String login) {
        try {
            return userDao.getByLogin(login) == null;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public Boolean logout(String uuid) {
        try {
            userDao.deleteUid(uuid);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public Boolean passwordRecovery(String login) {
        try {
            User user = userDao.getByLogin(login);
            if (user != null) {
                RandomStringUtils randomStringUtils = new RandomStringUtils();
                String newPassword = randomStringUtils.randomAlphanumeric(12);
                if (userDao.updatePassword(login, newPassword)) {
                    try {
                        new EmailSender().send(login, newPassword, user.getEmail());
                        return true;
                    } catch (Exception e) {
                        log.error(e.toString());
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public UserDTO getByLogin(String login) {
        try {
            return UserConverter.convertToUserDTO(userDao.getByLogin(login));
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }

    @Override
    public Boolean update(UserDTO userDTO) {
        try {
            User user = userDao.getByLogin(userDTO.getLogin());
            user.setEmail(userDTO.getEmail());
            user.setName(userDTO.getFirstName());
            user.setSurname(userDTO.getLastName());

            userDao.updateUser(user);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }

    @Override
    public Boolean updatePassword(String login, String newPassword) {
        try {
            userDao.updatePassword(login, newPassword);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
    }
}
