package by.grsu.util;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;

import java.io.FileInputStream;
import java.io.InputStream;


public class DropBoxSender {

    private static final String ACCESS_TOKEN = "k1XQ6z3l5nAAAAAAAAAABpiAbUYl5aYve3zX6H77DzUXPRfStD-829B1IvSLZwlQ";

    public static void main(String[] args) {
        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("BMSS");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

        try {
            // Get current account info
            FullAccount account = client.users().getCurrentAccount();
            System.out.println(account.getName().getDisplayName());


            try (InputStream in = new FileInputStream("src/main/java/by/grsu/util/test.txt")) {
                FileMetadata metadata = client.files().uploadBuilder("/test1.txt")
                        .uploadAndFinish(in);
                System.out.println(metadata);
            } catch (Exception e) {
                System.out.println(e.toString());
            }

            // Get files and folder metadata from Dropbox root directory
            ListFolderResult result = client.files().listFolder("");
            while (true) {
                for (Metadata metadata : result.getEntries()) {
                    System.out.println(metadata);
                    System.out.println(metadata.getPathLower());
                }

                if (!result.getHasMore()) {
                    break;
                }

                result = client.files().listFolderContinue(result.getCursor());
            }
        } catch (Exception e) {

        }

    }

}
