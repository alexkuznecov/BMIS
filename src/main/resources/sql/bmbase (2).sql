-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 05 2017 г., 07:50
-- Версия сервера: 5.7.18-0ubuntu0.16.04.1
-- Версия PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bmbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Attachment`
--

CREATE TABLE `Attachment` (
  `attchid` int(11) NOT NULL,
  `roid` int(11) DEFAULT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `attach_file` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Attachment`
--

INSERT INTO `Attachment` (`attchid`, `roid`, `name`, `description`, `attach_file`) VALUES
(1, 1, '03052017', '03052017', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `BuildingMaterial`
--

CREATE TABLE `BuildingMaterial` (
  `bmid` int(11) NOT NULL,
  `rotid` int(11) DEFAULT NULL,
  `mnfid` int(11) DEFAULT NULL,
  `mark` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `short_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `frost_resistance` varchar(130) COLLATE utf8_bin DEFAULT NULL,
  `mechanical_stress` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `made_year` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `spectr_file` varchar(500) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `BuildingMaterial`
--

INSERT INTO `BuildingMaterial` (`bmid`, `rotid`, `mnfid`, `mark`, `short_name`, `frost_resistance`, `mechanical_stress`, `made_year`, `spectr_file`) VALUES
(1, 1, 1, 'some1', 'BuildMaterial1', '123', '1234', '12.11.1122', '12345'),
(2, 2, 3, 'markk', 'BuuildMAterial2', '12345', '123', '12.11.1234', '123'),
(3, 4, 1, 'mmmar', 'BuildMAterial3', '1234', '123', '12.11.1123', '1211'),
(4, 3, 4, 'mark', 'BuildMaterial4', '12', '12', '12.11.2013', 'qwqwq'),
(6, 2, 2, '1', '3', '2', '5', '4', '6'),
(7, 3, 1, '111', 'Test_S_070417', '111', '999', '2016', 'file.txt'),
(8, 4, 2, NULL, '111', NULL, NULL, '2011', NULL),
(9, 2, 3, '1', '1', '1', '1', '1', '1'),
(10, 1, 2, '111', '333', '2222', '4444', '2011', '12');

-- --------------------------------------------------------

--
-- Структура таблицы `ChemicalElement`
--

CREATE TABLE `ChemicalElement` (
  `ceid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `symbol` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `atomic_weight` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `atomic_number` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `ions` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `group_number` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `period` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `density` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `melting_point` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `boiling_point` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `abundance` varchar(10) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `ChemicalElement`
--

INSERT INTO `ChemicalElement` (`ceid`, `name`, `symbol`, `atomic_weight`, `atomic_number`, `description`, `ions`, `group_number`, `period`, `density`, `melting_point`, `boiling_point`, `abundance`) VALUES
(1, 'Hydrogen', 'H', '1.0082 3 4 9', '1', 'the Greek \'hydro\' and \'genes\' meaning water-forming', '13.5984', '1', '1', '0.00008988', '14.01', '20.28', '1400'),
(2, 'Helium', 'He', '4.002602(2)2 4', '2', 'the Greek \'helios\' meaning sun', '24.5874', '18', '1', '0.0001785', '0.956', '4.22', '0.008'),
(3, 'Lithium', 'Li', '6.942 3 4 5 9', '3', 'the Greek \'lithos\' meaning stone', '5.3917', '1', '2', '0.534', '453.69', '1560', '20'),
(4, 'Beryllium', 'Be', '9.012182(3)', '4', 'the Greek name for beryl \'beryllo\'', '9.3227', '2', '2', '1.85', '1560', '2742', '2.8'),
(5, 'Boron', 'B', '10.812 3 4 9', '5', 'the Arabic \'buraq\' which was the name for borax', '8.298', '13', '2', '2.34', '2349', '4200', '10'),
(6, 'Carbon', 'C', '12.0112 4 9', '6', 'the Latin \'carbo\' meaning charcoal', '11.2603', '14', '2', '2.267', '3800', '4300', '200'),
(7, 'Nitrogen', 'N', '14.0072 4 9', '7', 'the Greek \'nitron\' and \'genes\' meaning nitre-forming', '14.5341', '15', '2', '0.0012506', '63.15', '77.36', '19'),
(8, 'Oxygen', 'O', '15.9992 4 9', '8', 'the Greek \'oxy\' and \'genes\' meaning acid-forming', '13.6181', '16', '2', '0.001429', '54.36', '90.2', '461000'),
(9, 'Fluorine', 'F', '18.9984032(5)', '9', 'the Latin \'fluere\' meaning to flow', '17.4228', '17', '2', '0.001696', '53.53', '85.03', '585'),
(10, 'Neon', 'Ne', '20.1797(6)2 3', '10', 'the Greek \'neos\' meaning new', '21.5645', '18', '2', '0.0008999', '24.56', '27.07', '0.005'),
(11, 'Sodium', 'Na', '22.98976928(2)', '11', 'the English word soda (natrium in Latin)', '5.1391', '1', '3', '0.971', '370.87', '1156', '23600'),
(12, 'Magnesium', 'Mg', '24.3059', '12', 'Magnesia a district of Eastern Thessaly in Greece', '7.6462', '2', '3', '1.738', '923', '1363', '23300'),
(13, 'Aluminium', 'Al', '26.9815386(8)', '13', 'the Latin name for alum \'alumen\' meaning bitter salt', '5.9858', '13', '3', '2.698', '933.47', '2792', '82300'),
(14, 'Silicon', 'Si', '28.0854 9', '14', 'the Latin \'silex\' or \'silicis\' meaning flint', '8.1517', '14', '3', '2.3296', '1687', '3538', '282000'),
(15, 'Phosphorus', 'P', '30.973762(2)', '15', 'the Greek \'phosphoros\' meaning bringer of light', '10.4867', '15', '3', '1.82', '317.3', '550', '1050'),
(16, 'Sulfur', 'S', '32.062 4 9', '16', 'Either from the Sanskrit \'sulvere\' or the Latin \'sulfurium\' both names for sulfur', '10.36', '16', '3', '2.067', '388.36', '717.87', '350'),
(17, 'Chlorine', 'Cl', '35.452 3 4 9', '17', 'the Greek \'chloros\' meaning greenish yellow', '12.9676', '17', '3', '0.003214', '171.6', '239.11', '145'),
(18, 'Argon', 'Ar', '39.948(1)2 4', '18', 'the Greek \'argos\' meaning idle', '15.7596', '18', '3', '0.0017837', '83.8', '87.3', '3.5'),
(19, 'Potassium', 'K', '39.0983(1)', '19', 'the English word potash (kalium in Latin)', '4.3407', '1', '4', '0.862', '336.53', '1032', '20900'),
(20, 'Calcium', 'Ca', '40.078(4)2', '20', 'the Latin \'calx\' meaning lime', '6.1132', '2', '4', '1.54', '1115', '1757', '41500'),
(21, 'Scandium', 'Sc', '44.955912(6)', '21', 'Scandinavia (with the Latin name Scandia)', '6.5615', '3', '4', '2.989', '1814', '3109', '22'),
(22, 'Titanium', 'Ti', '47.867(1)', '22', 'Titans the sons of the Earth goddess of Greek mythology', '6.8281', '4', '4', '4.54', '1941', '3560', '5650'),
(23, 'Vanadium', 'V', '50.9415(1)', '23', 'Vanadis an old Norse name for the Scandinavian goddess Freyja', '6.7462', '5', '4', '6.11', '2183', '3680', '120'),
(24, 'Chromium', 'Cr', '51.9961(6)', '24', 'the Greek \'chroma\' meaning colour', '6.7665', '6', '4', '7.15', '2180', '2944', '102'),
(25, 'Manganese', 'Mn', '54.938045(5)', '25', 'Either the Latin \'magnes\' meaning magnet or from the black magnesium oxid \'magnesia nigra\'', '7.434', '7', '4', '7.44', '1519', '2334', '950'),
(26, 'Iron', 'Fe', '55.845(2)', '26', 'the Anglo-Saxon name iren (ferrum in Latin)', '7.9024', '8', '4', '7.874', '1811', '3134', '56300'),
(27, 'Cobalt', 'Co', '58.933195(5)', '27', 'the German word \'kobald\' meaning goblin', '7.881', '9', '4', '8.86', '1768', '3200', '25'),
(28, 'Nickel', 'Ni', '58.6934(4)', '28', 'the shortened of the German \'kupfernickel\' meaning either devil\'s copper or St. Nicholas\'s copper', '7.6398', '10', '4', '8.912', '1728', '3186', '84'),
(29, 'Copper', 'Cu', '63.546(3)4', '29', 'the Old English name coper in turn derived from the Latin \'Cyprium aes\' meaning a metal from Cyprus', '7.7264', '11', '4', '8.96', '1357.77', '2835', '60'),
(30, 'Zinc', 'Zn', '65.38(2)', '30', 'the German \'zinc\' which may in turn be derived from the Persian word \'sing\' meaning stone', '9.3942', '12', '4', '7.134', '692.88', '1180', '70'),
(31, 'Gallium', 'Ga', '69.723(1)', '31', 'France (with the Latin name Gallia)', '5.9993', '13', '4', '5.907', '302.9146', '2477', '19'),
(32, 'Germanium', 'Ge', '72.630(8)', '32', 'Germany (with the Latin name Germania)', '7.8994', '14', '4', '5.323', '1211.4', '3106', '1.5'),
(33, 'Arsenic', 'As', '74.92160(2)', '33', 'the Greek name \'arsenikon\' for the yellow pigment orpiment', '9.7886', '15', '4', '5.776', '1090 7', '887', '1.8'),
(34, 'Selenium', 'Se', '78.96(3)4', '34', 'Moon (with the Greek name selene)', '9.7524', '16', '4', '4.809', '453', '958', '0.05'),
(35, 'Bromine', 'Br', '79.9049', '35', 'the Greek \'bromos\' meaning stench', '11.8138', '17', '4', '3.122', '265.8', '332', '2.4'),
(36, 'Krypton', 'Kr', '83.798(2)2 3', '36', 'the Greek \'kryptos\' meaning hidden', '13.9996', '18', '4', '0.003733', '115.79', '119.93', '<0.001'),
(37, 'Rubidium', 'Rb', '85.4678(3)2', '37', 'the Latin \'rubidius\' meaning deepest red', '4.1771', '1', '5', '1.532', '312.46', '961', '90'),
(38, 'Strontium', 'Sr', '87.62(1)2 4', '38', 'Strontian a small town in Scotland', '5.6949', '2', '5', '2.64', '1050', '1655', '370'),
(39, 'Yttrium', 'Y', '88.90585(2)', '39', 'Ytterby Sweden', '6.2173', '3', '5', '4.469', '1799', '3609', '33'),
(40, 'Zirconium', 'Zr', '91.224(2)2', '40', 'the Persian \'zargun\' meaning gold coloured', '6.6339', '4', '5', '6.506', '2128', '4682', '165'),
(41, 'Niobium', 'Nb', '92.90638(2)', '41', 'Niobe daughter of king Tantalus from Greek mythology', '6.7589', '5', '5', '8.57', '2750', '5017', '20'),
(42, 'Molybdenum', 'Mo', '95.96(2)2', '42', 'the Greek \'molybdos\' meaning lead', '7.0924', '6', '5', '10.22', '2896', '4912', '1.2'),
(43, 'Technetium', 'Tc', '[98]1', '43', 'the Greek \'tekhnetos\' meaning artificial', '7.28', '7', '5', '11.5', '2430', '4538', '<0.001'),
(44, 'Ruthenium', 'Ru', '101.07(2)2', '44', 'Russia (with the Latin name Ruthenia)', '7.3605', '8', '5', '12.37', '2607', '4423', '0.001'),
(45, 'Rhodium', 'Rh', '102.90550(2)', '45', 'the Greek \'rhodon\' meaning rose coloured', '7.4589', '9', '5', '12.41', '2237', '3968', '0.001'),
(46, 'Palladium', 'Pd', '106.42(1)2', '46', 'the then recently discovered asteroid Pallas considered a planet at the time', '8.3369', '10', '5', '12.02', '1828.05', '3236', '0.015'),
(47, 'Silver', 'Ag', '107.8682(2)2', '47', 'the Anglo-Saxon name siolfur (argentum in Latin)', '7.5762', '11', '5', '10.501', '1234.93', '2435', '0.075'),
(48, 'Cadmium', 'Cd', '112.411(8)2', '48', 'the Latin name for the mineral calmine \'cadmia\'', '8.9938', '12', '5', '8.69', '594.22', '1040', '0.159'),
(49, 'Indium', 'In', '114.818(1)', '49', 'the Latin \'indicium\' meaning violet or indigo', '5.7864', '13', '5', '7.31', '429.75', '2345', '0.25'),
(50, 'Tin', 'Sn', '118.710(7)2', '50', 'the Anglo-Saxon word tin (stannum in Latin meaning hard)', '7.3439', '14', '5', '7.287', '505.08', '2875', '2.3'),
(51, 'Antimony', 'Sb', '121.760(1)2', '51', 'the Greek \'anti � monos\' meaning not alone (stibium in Latin)', '8.6084', '15', '5', '6.685', '903.78', '1860', '0.2'),
(52, 'Tellurium', 'Te', '127.60(3)2', '52', 'Earth the third planet on solar system (with the Latin word tellus)', '9.0096', '16', '5', '6.232', '722.66', '1261', '0.001'),
(53, 'Iodine', 'I', '126.90447(3)', '53', 'the Greek \'iodes\' meaning violet', '10.4513', '17', '5', '4.93', '386.85', '457.4', '0.45'),
(54, 'Xenon', 'Xe', '131.293(6)2 3', '54', 'the Greek \'xenos\' meaning stranger', '12.1298', '18', '5', '0.005887', '161.4', '165.03', '<0.001'),
(55, 'Caesium', 'Cs', '132.9054519(2)', '55', 'the Latin \'caesius\' meaning sky blue', '3.8939', '1', '6', '1.873', '301.59', '944', '3'),
(56, 'Barium', 'Ba', '137.327(7)', '56', 'the Greek \'barys\' meaning heavy', '5.2117', '2', '6', '3.594', '1000', '2170', '425'),
(57, 'Lanthanum', 'La', '138.90547(7)2', '57', 'the Greek \'lanthanein\' meaning to lie hidden', '5.5769', '', '6', '6.145', '1193', '3737', '39'),
(58, 'Cerium', 'Ce', '140.116(1)2', '58', 'Ceres the Roman God of agriculture', '5.5387', '', '6', '6.77', '1068', '3716', '66.5'),
(59, 'Praseodymium', 'Pr', '140.90765(2)', '59', 'the Greek \'prasios didymos\' meaning green twin', '5.473', '', '6', '6.773', '1208', '3793', '9.2'),
(60, 'Neodymium', 'Nd', '144.242(3)2', '60', 'the Greek \'neos didymos\' meaning new twin', '5.525', '', '6', '7.007', '1297', '3347', '41.5'),
(61, 'Promethium', 'Pm', '[145]1', '61', 'Prometheus of Greek mythology who stole fire from the Gods and gave it to humans', '5.582', '', '6', '7.26', '1315', '3273', '<0.001'),
(62, 'Samarium', 'Sm', '150.36(2)2', '62', 'Samarskite the name of the mineral from which it was first isolated', '5.6437', '', '6', '7.52', '1345', '2067', '7.05'),
(63, 'Europium', 'Eu', '151.964(1)2', '63', 'Europe', '5.6704', '', '6', '5.243', '1099', '1802', '2'),
(64, 'Gadolinium', 'Gd', '157.25(3)2', '64', 'Johan Gadolin chemist physicist and mineralogist', '6.1501', '', '6', '7.895', '1585', '3546', '6.2'),
(65, 'Terbium', 'Tb', '158.92535(2)', '65', 'Ytterby Sweden', '5.8638', '', '6', '8.229', '1629', '3503', '1.2'),
(66, 'Dysprosium', 'Dy', '162.500(1)2', '66', 'the Greek \'dysprositos\' meaning hard to get', '5.9389', '', '6', '8.55', '1680', '2840', '5.2'),
(67, 'Holmium', 'Ho', '164.93032(2)', '67', 'Stockholm Sweden (with the Latin name Holmia)', '6.0215', '', '6', '8.795', '1734', '2993', '1.3'),
(68, 'Erbium', 'Er', '167.259(3)2', '68', 'Ytterby Sweden', '6.1077', '', '6', '9.066', '1802', '3141', '3.5'),
(69, 'Thulium', 'Tm', '168.93421(2)', '69', 'Thule the ancient name for Scandinavia', '6.1843', '', '6', '9.321', '1818', '2223', '0.52'),
(70, 'Ytterbium', 'Yb', '173.054(5)2', '70', 'Ytterby Sweden', '6.2542', '', '6', '6.965', '1097', '1469', '3.2'),
(71, 'Lutetium', 'Lu', '174.9668(1)2', '71', 'Paris France (with the Roman name Lutetia)', '5.4259', '3', '6', '9.84', '1925', '3675', '0.8'),
(72, 'Hafnium', 'Hf', '178.49(2)', '72', 'Copenhagen Denmark (with the Latin name Hafnia)', '6.8251', '4', '6', '13.31', '2506', '4876', '3'),
(73, 'Tantalum', 'Ta', '180.94788(2)', '73', 'King Tantalus father of Niobe from Greek mythology', '7.5496', '5', '6', '16.654', '3290', '5731', '2'),
(74, 'Tungsten', 'W', '183.84(1)', '74', 'the Swedish \'tung sten\' meaning heavy stone (W is wolfram the old name of the tungsten mineral wolframite)', '7.864', '6', '6', '19.25', '3695', '5828', '1.3'),
(75, 'Rhenium', 'Re', '186.207(1)', '75', 'Rhine a river that flows from Grisons in the eastern Swiss Alps to the North Sea coast in the Netherlands (with the Latin name Rhenia)', '7.8335', '7', '6', '21.02', '3459', '5869', '<0.001'),
(76, 'Osmium', 'Os', '190.23(3)2', '76', 'the Greek \'osme\' meaning smell', '8.4382', '8', '6', '22.61', '3306', '5285', '0.002'),
(77, 'Iridium', 'Ir', '192.217(3)', '77', 'Iris the Greek goddess of the rainbow', '8.967', '9', '6', '22.56', '2719', '4701', '0.001'),
(78, 'Platinum', 'Pt', '195.084(9)', '78', 'the Spanish \'platina\' meaning little silver', '8.9587', '10', '6', '21.46', '2041.4', '4098', '0.005'),
(79, 'Gold', 'Au', '196.966569(4)', '79', 'the Anglo-Saxon word gold (aurum in Latin meaning glow of sunrise)', '9.2255', '11', '6', '19.282', '1337.33', '3129', '0.004'),
(80, 'Mercury', 'Hg', '200.592(3)', '80', 'Mercury the first planet in the Solar System (Hg from former name hydrargyrum from Greek hydr- water and argyros silver)', '10.4375', '12', '6', '13.5336', '234.43', '629.88', '0.085'),
(81, 'Thallium', 'Tl', '204.389', '81', 'the Greek \'thallos\' meaning a green twig', '6.1082', '13', '6', '11.85', '577', '1746', '0.85'),
(82, 'Lead', 'Pb', '207.2(1)2 4', '82', 'the Anglo-Saxon lead (plumbum in Latin)', '7.4167', '14', '6', '11.342', '600.61', '2022', '14'),
(83, 'Bismuth', 'Bi', '208.98040(1)1', '83', 'the German \'Bisemutum\' a corruption of \'Weisse Masse\' meaning white mass', '7.2856', '15', '6', '9.807', '544.7', '1837', '0.009'),
(84, 'Polonium', 'Po', '[209]1', '84', 'Poland the native country of Marie Curie who first isolated the element', '8.417', '16', '6', '9.32', '527', '1235', '<0.001'),
(85, 'Astatine', 'At', '[210]1', '85', 'the Greek \'astatos\' meaning unstable', '9.3', '17', '6', '7', '575', '610', '<0.001'),
(86, 'Radon', 'Rn', '[222]1', '86', 'From radium as it was first detected as an emission from radium during radioactive decay', '10.7485', '18', '6', '0.00973', '202', '211.3', '<0.001'),
(87, 'Francium', 'Fr', '[223]1', '87', 'France where it was first discovered', '4.0727', '1', '7', '1.87', '300', '950', '<0.001'),
(88, 'Radium', 'Ra', '[226]1', '88', 'the Latin \'radius\' meaning ray', '5.2784', '2', '7', '5.5', '973', '2010', '<0.001'),
(89, 'Actinium', 'Ac', '[227]1', '89', 'the Greek \'actinos\' meaning a ray', '5.17', '', '7', '10.07', '1323', '3471', '<0.001'),
(90, 'Thorium', 'Th', '232.03806(2)1 2', '90', 'Thor the Scandinavian god of thunder', '6.3067', '', '7', '11.72', '2115', '5061', '9.6'),
(91, 'Protactinium', 'Pa', '231.03588(2)1', '91', 'the Greek \'protos\' meaning first as a prefix to the element actinium which is produced through the radioactive decay of protactinium', '5.89', '', '7', '15.37', '1841', '4300', '<0.001'),
(92, 'Uranium', 'U', '238.02891(3)1', '92', 'Uranus the seventh planet in the Solar System', '6.1941', '', '7', '18.95', '1405.3', '4404', '2.7'),
(93, 'Neptunium', 'Np', '[237]1', '93', 'Neptune the eighth planet in the Solar System', '6.2657', '', '7', '20.45', '917', '4273', '<0.001'),
(94, 'Plutonium', 'Pu', '[244]1', '94', 'Pluto a dwarf planet in the Solar System', '6.0262', '', '7', '19.84', '912.5', '3501', '<0.001'),
(95, 'Americium', 'Am', '[243]1', '95', 'Americas the continent where the element was first synthesized', '5.9738', '', '7', '13.69', '1449', '2880', '<0.001'),
(96, 'Curium', 'Cm', '[247]1', '96', 'Pierre Curie a physicist and Marie Curie a physicist and chemist', '5.9915', '', '7', '13.51', '1613', '3383', '<0.001'),
(97, 'Berkelium', 'Bk', '[247]1', '97', 'Berkeley California USA where the element was first synthesized', '6.1979', '', '7', '14.79', '1259', '2900', '<0.001'),
(98, 'Californium', 'Cf', '[251]1', '98', 'State of California USA where the element was first synthesized', '6.2817', '', '7', '15.1', '1173', '(1743)11', '<0.001'),
(99, 'Einsteinium', 'Es', '[252]1', '99', 'Albert Einstein physicist', '6.42', '', '7', '8.84', '1133', '(1269)11', '0 8'),
(100, 'Fermium', 'Fm', '[257]1', '100', 'Enrico Fermi physicist', '6.5', '', '7', '�', '(1125)11', '�', '0 8'),
(101, 'Mendelevium', 'Md', '[258]1', '101', 'Dmitri Mendeleyev chemist and inventor', '6.58', '', '7', '�', '(1100)11', '�', '0 8'),
(102, 'Nobelium', 'No', '[259]1', '102', 'Alfred Nobel chemist engineer innovator and armaments manufacturer', '6.65', '', '7', '�', '(1100)11', '�', '0 8'),
(103, 'Lawrencium', 'Lr', '[262]1', '103', 'Ernest O. Lawrence physicist', '4.9', '3', '7', '�', '(1900)11', '�', '0 8'),
(104, 'Rutherfordium', 'Rf', '[267]1', '104', 'Ernest Rutherford chemist and physicist', '', '4', '7', '(23.2)11', '(2400)11', '(5800)11', '0 8'),
(105, 'Dubnium', 'Db', '[268]1', '105', 'Dubna Russia', '', '5', '7', '(29.3)11', '�', '�', '0 8'),
(106, 'Seaborgium', 'Sg', '[269]1', '106', 'Glenn T. Seaborg scientist', '', '6', '7', '(35.0)11', '�', '�', '0 8'),
(107, 'Bohrium', 'Bh', '[270]1', '107', 'Niels Bohr physicist', '', '7', '7', '(37.1)11', '�', '�', '0 8'),
(108, 'Hassium', 'Hs', '[269]1', '108', 'Hesse Germany where the element was first synthesized', '', '8', '7', '(40.7)11', '�', '�', '0 8'),
(109, 'Meitnerium', 'Mt', '[278]1', '109', 'Lise Meitner physicist', '', '9', '7', '(37.4)11', '�', '�', '0 8'),
(110, 'Darmstadtium', 'Ds', '[281]1', '110', 'Darmstadz Germany where the element was first synthesized', NULL, '10', '7', '(34.8)11', '�', '�', '0 8'),
(111, 'Roentgenium', 'Rg', '[281]1', '111', 'Wilhelm Conrad R�ntgen physicist', NULL, '11', '7', '(28.7)11', '�', '�', '0 8'),
(112, 'Copernicium', 'Cn', '[285]1', '112', 'Nicolaus Copernicus astronomer', NULL, '12', '7', '(23.7)11', '�', '357 12', '0 8'),
(113, 'Ununtrium', 'Uut', '[286]1', '113', 'IUPAC systematic element name', NULL, '13', '7', '(16)11', '(700)11', '(1400)11', '0 8'),
(114, 'Flerovium', 'Fl', '[289]1', '114', 'Georgy Flyorov physicist', NULL, '14', '7', '(14)11', '(340)11', '(420)11', '0 8'),
(115, 'Ununpentium', 'Uup', '[288]1', '115', 'IUPAC systematic element name', NULL, '15', '7', '(13.5)11', '(700)11', '(1400)11', '0 8'),
(116, 'Livermorium', 'Lv', '[293]1', '116', 'Lawrence Livermore National Laboratory (in Livermore California) which collaborated with JINR on its synthesis', NULL, '16', '7', '(12.9)11', '(708.5)11', '(1085)11', '0 8'),
(117, 'Ununseptium', 'Uus', '[294]1', '117', 'IUPAC systematic element name', NULL, '17', '7', '(7.2)11', '(673)11', '(823)11', '0 8'),
(118, 'Ununoctium', 'Uuo', '[294]1', '118', 'IUPAC systematic element name', NULL, '18', '7', '(5.0)11 13', '(258)11', '(263)11', '0 8');

-- --------------------------------------------------------

--
-- Структура таблицы `Credentials`
--

CREATE TABLE `Credentials` (
  `id` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Credentials`
--

INSERT INTO `Credentials` (`id`, `login`, `password`) VALUES
(1, 'test', 'test'),
(2, 'slava', '123456'),
(3, 'usertest', '12345678'),
(4, 'slava1', '123456'),
(5, 'newUser', '123456'),
(6, 'Slaik', '12345678');

-- --------------------------------------------------------

--
-- Структура таблицы `EthalonSpectr`
--

CREATE TABLE `EthalonSpectr` (
  `etsid` int(11) NOT NULL,
  `mid` int(11) DEFAULT NULL,
  `bmid` int(11) DEFAULT NULL,
  `ceid` int(11) DEFAULT NULL,
  `slid` int(11) DEFAULT NULL,
  `wave_length` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `line_description` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `spectr_base` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `EthalonSpectr`
--

INSERT INTO `EthalonSpectr` (`etsid`, `mid`, `bmid`, `ceid`, `slid`, `wave_length`, `line_description`, `spectr_base`) VALUES
(1, 1, 2, 33, 4, '222', 'DescDescDescDesc1', 'qqqqq'),
(2, 2, 4, 22, 2, '55', 'DescDescDescDesc2', 'bbbase'),
(3, 5, 1, 44, 2, '22', 'DescDescDescDesc3', 'ddadsa'),
(4, 4, 3, 26, 4, '66', 'DescDescDescDesc4', 'dadasdas'),
(5, 1, 1, 1, 1, '123', 'etalon spectr test 1', '11231');

-- --------------------------------------------------------

--
-- Структура таблицы `File`
--

CREATE TABLE `File` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `File`
--

INSERT INTO `File` (`id`, `name`, `alias`, `type`) VALUES
(1, '/home/alek/Files/9f2a4c00-fb1d-49de-9ab1-ffa0e3c82e46.txt', '111', 'ATTACHMENT'),
(2, '/home/alek/Files/a4225b30-ca6a-41dd-850d-da778cda886b.txt', '111', 'ATTACHMENT'),
(3, '/home/alek/Files/2f94b8c6-cdfb-4f44-aafc-fc5d9eb96abf.txt', '111111', 'ATTACHMENT'),
(4, '/home/alek/Files/bef7f2b9-5b78-4821-8c5c-6230700f39f0.txt', '111111', 'SPECTR'),
(5, '/home/alek/Files/0287ce3c-d1a5-4799-bf09-d0a0200122c1.txt', '111111', 'RESEARCH_PASSPORT');

-- --------------------------------------------------------

--
-- Структура таблицы `Manufacturer`
--

CREATE TABLE `Manufacturer` (
  `mnfid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `year_of_creation` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Manufacturer`
--

INSERT INTO `Manufacturer` (`mnfid`, `name`, `year_of_creation`, `description`) VALUES
(1, 'Manufacturer1', '2011', 'Manufacturer1'),
(2, 'Manufacturer2', '2012', 'Manufacturer2'),
(3, 'Manufacturer3', '2013', 'Manufacturer3'),
(4, 'Manufacturer4', '2014', 'Manufacturer4'),
(5, 'AA', '2017', 'AA'),
(6, '03052017', '2017', '03052017');

-- --------------------------------------------------------

--
-- Структура таблицы `Material`
--

CREATE TABLE `Material` (
  `mid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `probe_date` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `probe_place` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `spectr_file` varchar(500) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Material`
--

INSERT INTO `Material` (`mid`, `name`, `probe_date`, `probe_place`, `description`, `spectr_file`) VALUES
(1, 'test1', '11.12.2012', 'Grodno', 'SomeDesc', '1'),
(2, 'test1', '11.12.2013', 'Grodno', 'SomeDesc1', '2'),
(3, 'Бетон', '11.02.1995', 'Гродно', 'Это Бетон и он Бетон', 'ффффф'),
(4, 'Дым', '11.03.1944', 'Где-то', 'Это дым и он где-то', '1'),
(5, 'Смола', '25.04.2012', 'Орша', 'Тут тут тут', 'фффф\r\n'),
(6, 'Акрил', '25.04.2012', 'Минск', 'И тут тут тут', 'фвыфвфвфы'),
(7, '123', '20.04.2017', '111', 'material test 1', '11111');

-- --------------------------------------------------------

--
-- Структура таблицы `Material_has_ElementSource`
--

CREATE TABLE `Material_has_ElementSource` (
  `id` int(11) NOT NULL,
  `bmid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Material_has_ElementSource`
--

INSERT INTO `Material_has_ElementSource` (`id`, `bmid`, `mid`) VALUES
(1, 6, 1),
(2, 6, 3),
(3, 6, 5),
(4, 7, 3),
(5, 7, 5),
(6, 7, 1),
(7, 9, 2),
(8, 9, 3),
(9, 10, 2),
(10, 10, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `Material_has_QualityStandart`
--

CREATE TABLE `Material_has_QualityStandart` (
  `id` int(11) NOT NULL,
  `qsid` int(11) DEFAULT NULL,
  `bmid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Material_has_QualityStandart`
--

INSERT INTO `Material_has_QualityStandart` (`id`, `qsid`, `bmid`) VALUES
(1, 3, 6),
(2, 1, 6),
(3, 1, 7),
(4, 1, 9),
(5, 3, 9),
(6, 2, 10),
(7, 1, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `Organization`
--

CREATE TABLE `Organization` (
  `oid` int(11) NOT NULL,
  `full_title` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `short_title` varchar(60) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Organization`
--

INSERT INTO `Organization` (`oid`, `full_title`, `short_title`) VALUES
(1, 'Organization1', 'Organization1'),
(2, 'Organization2', 'Organization2'),
(3, 'Organization3', 'Organization3'),
(4, 'Organization4', 'Organization4'),
(5, 'AA', 'AAA'),
(6, '03052017', '0305201703052017');

-- --------------------------------------------------------

--
-- Структура таблицы `QualityStandart`
--

CREATE TABLE `QualityStandart` (
  `qsid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `QualityStandart`
--

INSERT INTO `QualityStandart` (`qsid`, `name`) VALUES
(1, 'qstandart1'),
(2, 'qstandart2'),
(3, 'qstandart3'),
(4, 'qstandart4'),
(5, 'AA'),
(6, '03052017');

-- --------------------------------------------------------

--
-- Структура таблицы `ResearchMethod`
--

CREATE TABLE `ResearchMethod` (
  `rmid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `ResearchMethod`
--

INSERT INTO `ResearchMethod` (`rmid`, `name`, `description`) VALUES
(1, 'ResearchMethod1', 'ResearchMethod1'),
(2, 'ResearchMethod2', 'ResearchMethod2'),
(3, 'ResearchMethod3', 'ResearchMethod3'),
(4, 'ResearchMethod4', 'ResearchMethod4'),
(5, 'AA', 'Aaa'),
(6, '03052017', '03052017');

-- --------------------------------------------------------

--
-- Структура таблицы `ResearchObject`
--

CREATE TABLE `ResearchObject` (
  `roid` int(11) NOT NULL,
  `oid` int(11) DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `ResearchObject`
--

INSERT INTO `ResearchObject` (`roid`, `oid`, `description`, `date`, `name`) VALUES
(1, 1, 'ResearchObject1', '11.11.2011', 'ResearchObject1'),
(2, 2, 'ResearchObject2', '11.11.2012', ' ResearchObject2'),
(3, 3, 'ResearchObject3', '11.11.2013', 'ResearchObject1'),
(4, 4, 'ResearchObject4', '11.11.2014', 'ResearchObject4'),
(5, 1, 'res object test 1', '2017-04-1', '123'),
(6, 1, 'res object test 2', '15.04.2017', '1233');

-- --------------------------------------------------------

--
-- Структура таблицы `ResearchObjectType`
--

CREATE TABLE `ResearchObjectType` (
  `rotid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `ResearchObjectType`
--

INSERT INTO `ResearchObjectType` (`rotid`, `name`) VALUES
(1, 'ResearchObjectType1'),
(2, 'ResearchObjectType2'),
(3, 'ResearchObjectType3'),
(4, 'ResearchObjectType4\r\n'),
(5, 'AA'),
(6, '03052017');

-- --------------------------------------------------------

--
-- Структура таблицы `ResearchPassport`
--

CREATE TABLE `ResearchPassport` (
  `rpid` int(11) NOT NULL,
  `rmid` int(11) DEFAULT NULL,
  `roid` int(11) DEFAULT NULL,
  `spectr_file` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `intensity` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `ResearchPassport`
--

INSERT INTO `ResearchPassport` (`rpid`, `rmid`, `roid`, `spectr_file`, `description`, `intensity`) VALUES
(1, 1, 2, 'qwer', 'description1', '123'),
(2, 2, 1, 'qwerty', 'description2', '333'),
(3, 3, 4, 'rtyu', 'description3', '333'),
(4, 4, 3, 'eeeee', 'description4', '12'),
(5, 1, 2, NULL, '03052017', '03052017');

-- --------------------------------------------------------

--
-- Структура таблицы `Spectr`
--

CREATE TABLE `Spectr` (
  `spcid` int(11) NOT NULL,
  `rpid` int(11) DEFAULT NULL,
  `slid` int(11) DEFAULT NULL,
  `ceid` int(11) DEFAULT NULL,
  `wave_length` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `line_description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `spectr_base` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Spectr`
--

INSERT INTO `Spectr` (`spcid`, `rpid`, `slid`, `ceid`, `wave_length`, `line_description`, `spectr_base`) VALUES
(1, 1, 2, 1, '12', 'DescriptionDesc1', 'aaaaa'),
(2, 2, 1, 3, '11', 'DescriptionDesc2', 'asdasdas'),
(3, 3, 4, 45, '444', 'DescriptionDesc3', 'bbbase'),
(4, 4, 3, 65, '55', 'DescriptionDesc4', 'asdasdas'),
(5, 1, 1, 1, '123', 'test spectr 1', '1111');

-- --------------------------------------------------------

--
-- Структура таблицы `SpectrLine`
--

CREATE TABLE `SpectrLine` (
  `slid` int(11) NOT NULL,
  `sllid` int(11) DEFAULT NULL,
  `date_line` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `person_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `wavelength_line` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `other_option` varchar(150) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `SpectrLine`
--

INSERT INTO `SpectrLine` (`slid`, `sllid`, `date_line`, `person_name`, `wavelength_line`, `other_option`) VALUES
(1, 2, '11.11.2011', 'Name1', '2334', 'optrion'),
(2, 1, '11.11.2012', 'Name2', '222', 'qweqw'),
(3, 3, '11.11.2013', 'Name3', '222', 'option'),
(4, 4, '11.11.2014', 'naaame4', '1234', '111');

-- --------------------------------------------------------

--
-- Структура таблицы `SpectrLineLibrary`
--

CREATE TABLE `SpectrLineLibrary` (
  `sllid` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `comments_library` varchar(180) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `SpectrLineLibrary`
--

INSERT INTO `SpectrLineLibrary` (`sllid`, `name`, `description`, `comments_library`) VALUES
(1, 'SpectrLineLibrary1', 'dessasdafafas', 'daddesfsdfds'),
(2, 'SpectrLineLibrary2', 'asdasdasds', 'asfdfsfsd'),
(3, 'SpectrLineLibrary3', 'adasdasdas', 'dadadadas'),
(4, 'SpectrLineLibrary4', 'adasdasdas', 'afafafasfa');

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `surname` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`id`, `login`, `email`, `name`, `surname`) VALUES
(1, 'test', 'test@test.tes', 'test', 'test'),
(2, 'slava', 'mail@mail.ru', 'Viacheslav', 'Voronovich'),
(3, 'usertest', 'mail@mail.ru', 'user', 'test'),
(4, 'slava1', 'maila@mail.ru', 'slava', 'voron'),
(5, 'newUser', 'user@mail.ru', 'new', 'user'),
(6, 'Slaik', 'slongtong@mail.ru', 'Viacheslav', 'Voronovich');

-- --------------------------------------------------------

--
-- Структура таблицы `Uuid`
--

CREATE TABLE `Uuid` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `uuid` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `Uuid`
--

INSERT INTO `Uuid` (`id`, `userId`, `uuid`) VALUES
(2, 2, '1c2d63b7-ff5a-4d92-aacc-e6a3aee2f6f7'),
(6, 5, '2de64261-e6a9-4d10-ab48-fde11656e23c'),
(7, 1, 'd8a99230-83e9-4df4-bf9c-bcad8a176285');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Attachment`
--
ALTER TABLE `Attachment`
  ADD PRIMARY KEY (`attchid`),
  ADD KEY `idx_Attachment` (`roid`);

--
-- Индексы таблицы `BuildingMaterial`
--
ALTER TABLE `BuildingMaterial`
  ADD PRIMARY KEY (`bmid`),
  ADD KEY `idx_BuildingMaterial` (`rotid`),
  ADD KEY `idx_BuildingMaterial_0` (`mnfid`);

--
-- Индексы таблицы `ChemicalElement`
--
ALTER TABLE `ChemicalElement`
  ADD PRIMARY KEY (`ceid`);

--
-- Индексы таблицы `Credentials`
--
ALTER TABLE `Credentials`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `EthalonSpectr`
--
ALTER TABLE `EthalonSpectr`
  ADD PRIMARY KEY (`etsid`),
  ADD KEY `idx_EthalonSpectr` (`bmid`),
  ADD KEY `idx_EthalonSpectr_0` (`mid`),
  ADD KEY `idx_EthalonSpectr_1` (`ceid`),
  ADD KEY `idx_EthalonSpectr_2` (`slid`);

--
-- Индексы таблицы `File`
--
ALTER TABLE `File`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Manufacturer`
--
ALTER TABLE `Manufacturer`
  ADD PRIMARY KEY (`mnfid`);

--
-- Индексы таблицы `Material`
--
ALTER TABLE `Material`
  ADD PRIMARY KEY (`mid`);

--
-- Индексы таблицы `Material_has_ElementSource`
--
ALTER TABLE `Material_has_ElementSource`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_Material_has_ElementSource` (`mid`),
  ADD KEY `idx_Material_has_ElementSource_0` (`bmid`);

--
-- Индексы таблицы `Material_has_QualityStandart`
--
ALTER TABLE `Material_has_QualityStandart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_Material_has_QualityStandart` (`qsid`),
  ADD KEY `idx_Material_has_QualityStandart_0` (`bmid`);

--
-- Индексы таблицы `Organization`
--
ALTER TABLE `Organization`
  ADD PRIMARY KEY (`oid`);

--
-- Индексы таблицы `QualityStandart`
--
ALTER TABLE `QualityStandart`
  ADD PRIMARY KEY (`qsid`);

--
-- Индексы таблицы `ResearchMethod`
--
ALTER TABLE `ResearchMethod`
  ADD PRIMARY KEY (`rmid`);

--
-- Индексы таблицы `ResearchObject`
--
ALTER TABLE `ResearchObject`
  ADD PRIMARY KEY (`roid`),
  ADD KEY `idx_ResearchObject` (`oid`);

--
-- Индексы таблицы `ResearchObjectType`
--
ALTER TABLE `ResearchObjectType`
  ADD PRIMARY KEY (`rotid`);

--
-- Индексы таблицы `ResearchPassport`
--
ALTER TABLE `ResearchPassport`
  ADD PRIMARY KEY (`rpid`),
  ADD KEY `idx_ResearchPassport` (`roid`),
  ADD KEY `idx_ResearchPassport_0` (`rmid`);

--
-- Индексы таблицы `Spectr`
--
ALTER TABLE `Spectr`
  ADD PRIMARY KEY (`spcid`),
  ADD KEY `idx_Spectr` (`rpid`),
  ADD KEY `idx_Spectr_0` (`slid`),
  ADD KEY `idx_Spectr_1` (`ceid`);

--
-- Индексы таблицы `SpectrLine`
--
ALTER TABLE `SpectrLine`
  ADD PRIMARY KEY (`slid`),
  ADD KEY `idx_SpectrLine` (`sllid`);

--
-- Индексы таблицы `SpectrLineLibrary`
--
ALTER TABLE `SpectrLineLibrary`
  ADD PRIMARY KEY (`sllid`);

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Uuid`
--
ALTER TABLE `Uuid`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Attachment`
--
ALTER TABLE `Attachment`
  MODIFY `attchid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `BuildingMaterial`
--
ALTER TABLE `BuildingMaterial`
  MODIFY `bmid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `ChemicalElement`
--
ALTER TABLE `ChemicalElement`
  MODIFY `ceid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT для таблицы `Credentials`
--
ALTER TABLE `Credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `EthalonSpectr`
--
ALTER TABLE `EthalonSpectr`
  MODIFY `etsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `File`
--
ALTER TABLE `File`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `Manufacturer`
--
ALTER TABLE `Manufacturer`
  MODIFY `mnfid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `Material`
--
ALTER TABLE `Material`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `Material_has_ElementSource`
--
ALTER TABLE `Material_has_ElementSource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `Material_has_QualityStandart`
--
ALTER TABLE `Material_has_QualityStandart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `Organization`
--
ALTER TABLE `Organization`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `QualityStandart`
--
ALTER TABLE `QualityStandart`
  MODIFY `qsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `ResearchMethod`
--
ALTER TABLE `ResearchMethod`
  MODIFY `rmid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `ResearchObject`
--
ALTER TABLE `ResearchObject`
  MODIFY `roid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `ResearchObjectType`
--
ALTER TABLE `ResearchObjectType`
  MODIFY `rotid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `ResearchPassport`
--
ALTER TABLE `ResearchPassport`
  MODIFY `rpid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `Spectr`
--
ALTER TABLE `Spectr`
  MODIFY `spcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `SpectrLine`
--
ALTER TABLE `SpectrLine`
  MODIFY `slid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `SpectrLineLibrary`
--
ALTER TABLE `SpectrLineLibrary`
  MODIFY `sllid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `Uuid`
--
ALTER TABLE `Uuid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Attachment`
--
ALTER TABLE `Attachment`
  ADD CONSTRAINT `fk_attachment_researchobject` FOREIGN KEY (`roid`) REFERENCES `ResearchObject` (`roid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `BuildingMaterial`
--
ALTER TABLE `BuildingMaterial`
  ADD CONSTRAINT `fk_buildingmaterial` FOREIGN KEY (`rotid`) REFERENCES `ResearchObjectType` (`rotid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_buildingmaterial1` FOREIGN KEY (`mnfid`) REFERENCES `Manufacturer` (`mnfid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `EthalonSpectr`
--
ALTER TABLE `EthalonSpectr`
  ADD CONSTRAINT `fk_ethalonspectr` FOREIGN KEY (`bmid`) REFERENCES `BuildingMaterial` (`bmid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ethalonspectr2` FOREIGN KEY (`ceid`) REFERENCES `ChemicalElement` (`ceid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ethalonspectr_material` FOREIGN KEY (`mid`) REFERENCES `Material` (`mid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ethalonspectr_spectrline` FOREIGN KEY (`slid`) REFERENCES `SpectrLine` (`slid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `Material_has_ElementSource`
--
ALTER TABLE `Material_has_ElementSource`
  ADD CONSTRAINT `fk_material_has_elementsource` FOREIGN KEY (`mid`) REFERENCES `Material` (`mid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_material_has_elementsource1` FOREIGN KEY (`bmid`) REFERENCES `BuildingMaterial` (`bmid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `Material_has_QualityStandart`
--
ALTER TABLE `Material_has_QualityStandart`
  ADD CONSTRAINT `fk_material_has_qualitystandart` FOREIGN KEY (`qsid`) REFERENCES `QualityStandart` (`qsid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_material_has_qualitystandart1` FOREIGN KEY (`bmid`) REFERENCES `BuildingMaterial` (`bmid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ResearchObject`
--
ALTER TABLE `ResearchObject`
  ADD CONSTRAINT `fk_researchobject_organization` FOREIGN KEY (`oid`) REFERENCES `Organization` (`oid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ResearchPassport`
--
ALTER TABLE `ResearchPassport`
  ADD CONSTRAINT `fk_researchpassport` FOREIGN KEY (`roid`) REFERENCES `ResearchObject` (`roid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_researchpassport1` FOREIGN KEY (`rmid`) REFERENCES `ResearchMethod` (`rmid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `Spectr`
--
ALTER TABLE `Spectr`
  ADD CONSTRAINT `fk_spectr_chemicalelement` FOREIGN KEY (`ceid`) REFERENCES `ChemicalElement` (`ceid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_spectr_researchpassport` FOREIGN KEY (`rpid`) REFERENCES `ResearchPassport` (`rpid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_spectr_spectrline` FOREIGN KEY (`slid`) REFERENCES `SpectrLine` (`slid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `SpectrLine`
--
ALTER TABLE `SpectrLine`
  ADD CONSTRAINT `fk_spectrline` FOREIGN KEY (`sllid`) REFERENCES `SpectrLineLibrary` (`sllid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
